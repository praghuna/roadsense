package com.samyak.roadsense;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by parthasarathyraghunathan on 11/06/15.
 */
public class ListViewAdapter extends BaseAdapter implements Filterable {

    Context mContext;
    int layoutResourceId;
    ArrayList<JSONObject> data = null;
    ArrayList<JSONObject> filteredData = null;
    Hashtable<String, Double> mAgg;
    int property;

    public JSONObject getObjectAtPosition(int pos) {
        return filteredData.get(pos);
    }

    Filter mFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String filterString = charSequence.toString().toLowerCase();
            FilterResults results = new FilterResults();
            ArrayList<JSONObject> fildata = new ArrayList<JSONObject>(data.size());

            String filterableString ;
            for(int i = 0 ; i < data.size(); i++) {
                filterableString = data.get(i).toString();
                if (filterableString.toLowerCase().contains(filterString)) {
                    fildata.add(data.get(i));
                }
            }

            results.values = fildata;
            results.count = fildata.size();
            filteredData = (ArrayList<JSONObject>) results.values;

            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            filteredData = (ArrayList<JSONObject>) filterResults.values;
            notifyDataSetChanged();
        }

        public void filter(String cs) {

        }

    };

    public ListViewAdapter(Context mContext, int layoutResourceId, ArrayList<JSONObject> data, int property) {

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
        this.filteredData = data;
        this.property = property;

    }


    public Filter getFilter() {
        return mFilter;
    }

    public JSONObject getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public int getCount() {
        return filteredData.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /*
         * The convertView argument is essentially a "ScrapView" as described is Lucas post
         * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
         * It will have a non-null value when ListView is asking you recycle the row layout.
         * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
         */
        String type = "TYPE";

        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);


        if (property == 0) {  // list debit credit all and monthly
            // object item based on the position
            JSONObject objectItem = filteredData.get(position);
            String fn = "FILENAME";
            String dur = "DURATION";
            String stime = "START TIME";
            String etime = "END TIME";

            TextView fileName = (TextView) convertView.findViewById(R.id.fileName);
            TextView distance = (TextView) convertView.findViewById(R.id.distance);
            TextView startTime = (TextView) convertView.findViewById(R.id.startTime);
            TextView endTime = (TextView) convertView.findViewById(R.id.endTime);
            TextView duration = (TextView) convertView.findViewById(R.id.duration);

            try {
                if (objectItem.has("duration"))
                    dur = objectItem.getString("duration");

                if (objectItem.has("startTime"))
                    stime = objectItem.getString("startTime");

                if (objectItem.has("endTime"))
                    etime = objectItem.getString("endTime");

                if (objectItem.has("fileName")) {
                    fn = objectItem.getString("fileName");
                    File file = new File(fn);
                    // extract just the name of the file excluding the path.
                    fn = file.getName();
                }

            } catch (Exception ex) {
                System.out.println("ListViewAdapter:getView:" + ex.getLocalizedMessage());
            }
            // get the TextView and then set the text (item name) and tag (item ID) values
            //      TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewItem);
            fileName.setText(fn);

            startTime.setText(stime);
            endTime.setText(etime);
            duration.setText(dur);
            distance.setText("");

        }
        return convertView;

    }

}

