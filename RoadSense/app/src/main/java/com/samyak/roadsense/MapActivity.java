package com.samyak.roadsense;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;
import com.samyak.clapper.DetectorThread;
import com.samyak.clapper.OnSignalsDetectedListener;
import com.samyak.clapper.RecorderThread;
import com.samyak.roadsense.util.IOHttp;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint({ "NewApi", "SimpleDateFormat" })
public class MapActivity extends Activity  implements OnSignalsDetectedListener, LocationActivity, OnMarkerClickListener, OnMapReadyCallback {
  static final String FILE = "http://m.uploadedit.com/b026/1387987285344.txt";
  String g_file ;
  static final String g_file1 = "coordinates.txt";
  private String g_openFile;
	private boolean permissionGranted = false;
  RoadMap m_map;
	public static final int DETECT_NONE = 0;
	public static final int DETECT_WHISTLE = 1;
	public static final int RECORD_ACTIVITY_INTENT = 2;
	public static final int PLOT_PATH_INTENT = 3;
	public static int selectedDetection = DETECT_NONE;
    SensorBg m_sensor;

	public static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION=1;
	// detection parameters
	private DetectorThread detectorThread;
	private RecorderThread recorderThread;

	static MapActivity mainApp;

	private TextView timerText= null;
	private TextView speedText = null;
	private TextView progressText = null;
	private TextView recordCount = null;
	private	Button startButton;
	private	Button chooseFile ;
    private Button resetButton;
    private Button uploadButton;
    private ProgressBar progressBar;
	private Location m_setLocation = null;
    
    double prevLat = 0;
    double prevLong = 0;
	int [] recs;

    // invoked after an activity started by this activity has ended. Currently
    /// the only activity started by this activity is ACTION_GET_CONTENT (see below)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    
     switch(requestCode){
     case PLOT_PATH_INTENT:
      // if a file is selected plot it on the map. The sanity of the data in the
      // file is not checked. IMPORTANT !! Please select only valid files.
  	  if(resultCode==RESULT_OK){
        String FilePath = data.getStringExtra("fileName");
        g_openFile = FilePath;
        recs = IOHttp.plotFileCoordinates_abs(g_openFile, this ,true);
		recordCount.setText(recs[0]+"/"+recs[1]);
		recordCount.setText(recs[0]+"/"+recs[1]);
      }
      break;
     case RECORD_ACTIVITY_INTENT:
    	 if (resultCode == RESULT_CANCELED) {
    		   runOnUiThread( new Runnable() {
 	    	     public void run() {
    		        showSettingsAlert();
    		     }
    		   });
    	 }
     }
   }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    
    mainApp = this;
    
    setContentView(R.layout.activity_map);
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);

  MapFragment mapFragment = (MapFragment) getFragmentManager()
			  .findFragmentById(R.id.map);
	  mapFragment.getMapAsync(this);


	startButton = (Button)findViewById(R.id.start);
	chooseFile = (Button) findViewById(R.id.chooseFile);
    resetButton = (Button)findViewById(R.id.reset);
    uploadButton = (Button) findViewById(R.id.upload);
    
    timerText = (TextView) findViewById(R.id.timer);
    speedText = (TextView) findViewById(R.id.currentspeed);
    progressBar = (ProgressBar) findViewById(R.id.uploading);
    progressText = (TextView) findViewById(R.id.progressText) ;
	recordCount = (TextView) findViewById(R.id.recordcount);
 

    
    g_file = getString(R.string.bumps_filename);
    
    uploadButton.setOnClickListener(new OnClickListener() {
    	public void onClick(View arg0) {
    	  String uniq = new SimpleDateFormat("ddMMHHmmss").format(new Date())+
	    			       Secure.getString(getContentResolver(), Secure.ANDROID_ID)+ ".txt";


    		IOHttp.clearFile(g_file, uniq);
    		UploadFile uf  = new UploadFile(IOHttp.listFilesInDir("bumps"));
            uf.execute((Integer)null);
    	}
    });

      if ( Build.VERSION.SDK_INT >= 23 &&
              ContextCompat.checkSelfPermission(this.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
              ContextCompat.checkSelfPermission( this.getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

		  String [] perms = new String[5];
		  perms[0] = android.Manifest.permission.ACCESS_FINE_LOCATION;
		  perms[1] = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
		  perms[2] = android.Manifest.permission.READ_EXTERNAL_STORAGE;
		  perms[3] = Manifest.permission.ACCESS_NETWORK_STATE;
		  perms[4] = Manifest.permission.INTERNET;
		  ActivityCompat.requestPermissions(this,
				  perms,
				  MY_PERMISSIONS_ACCESS_FINE_LOCATION);
	  } 	else {
    	  permissionGranted = true;
		  init();
	  }



  }

    private void init() {
		startButton.setOnClickListener (new OnClickListener() {
			public void onClick(View arg0) {
				Intent myIntent = new Intent(MapActivity.this, RecordActivity.class);
				myIntent.putExtra("g_file", g_file.toString()); //Optional parameters
				startActivityForResult(myIntent, RECORD_ACTIVITY_INTENT);

//    		  MapActivity.this.startActivity(myIntent);
			}
		});

		// open file browser and use the file selected.
		// the plotting of the file will be done by onActivityResult() above.
		chooseFile.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent myIntent = new Intent(MapActivity.this, PathList.class);
				//	myIntent.putExtra("g_file", g_file.toString()); //Optional parameters
				startActivityForResult(myIntent, PLOT_PATH_INTENT);

//			boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
//			if (isKitKat) {
//				Intent intent = new Intent();
//				intent.setType("*/* ");
//				intent.setAction(Intent.ACTION_GET_CONTENT);
//				startActivityForResult(intent,0);
//
//			} else {
//				openFile("* / *");
//			}


				//Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);

				// fileIntent.setType("file/*"); // intent type to filter application based on your requirement
				// startActivityForResult(fileIntent,1 );

			}

		});
		if (m_sensor == null) {
			m_sensor = new SensorBg(this, g_file, getString(R.string.config_filename));
		}
		if (m_map != null) {
			m_map.moveCameratoLocation(m_sensor.getLatitude(), m_sensor.getLongitude());
			LatLng myLoc = new LatLng(m_sensor.getLatitude(), m_sensor.getLongitude());

			// Move the camera instantly to currentLoc with a zoom of 15.
			m_map.m_gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 15));


		}
	}
	public void onRequestPermissionsResult(int requestCode,
											  String[] permissions,
											  int[] grantResults){
		switch (requestCode) {
			case MY_PERMISSIONS_ACCESS_FINE_LOCATION:  {
				// If request is cancelled, the result arrays are empty.
				if ((grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

					// permission was granted, yay! Do the
					// contacts-related task you need to do.
					// setup click listeners for the buttons
 					permissionGranted = true;
					init();

				} else {

					// permission denied, boo! Disable the
					// functionality that depends on this permission.
					finish();

				}
				return;
			}

		}
	}
  public void useLocation(Location loc) {
	  if (m_map != null) {
		  m_map.moveCameratoLocation(loc.getLatitude(), loc.getLongitude());
	  } else {
		  m_setLocation = loc;
	  }
  }

	public void openFile(String minmeType) {

		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType(minmeType);
		intent.addCategory(Intent.CATEGORY_OPENABLE);

		// special intent for Samsung file manager
		Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
		// if you want any file type, you can skip next line
		sIntent.putExtra("CONTENT_TYPE", minmeType);
		sIntent.addCategory(Intent.CATEGORY_DEFAULT);

		Intent chooserIntent;
		if (getPackageManager().resolveActivity(sIntent, 0) != null){
			// it is device with samsung file manager
			chooserIntent = Intent.createChooser(sIntent, "Open file");
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
		}
		else {
			chooserIntent = Intent.createChooser(intent, "Open file");
		}

		try {
			startActivityForResult(chooserIntent, 0);
		} catch (android.content.ActivityNotFoundException ex) {
			//TODO add error alert
		}
	}
	public void onMapReady(GoogleMap map) {
		m_map = new RoadMap(map,"ic_launcher.png");
		m_map.m_gmap.setOnMarkerClickListener(this);

		resetButton.setOnClickListener(new OnClickListener(){

										   public void onClick(View arg0) {
											   try {
												   m_map.clearAllMarkers();
											   } catch(Exception exp) {

											   }
										   }
									   }
		);

		if (permissionGranted) {
			if (m_setLocation != null) {
 				m_map.moveCameratoLocation(m_setLocation.getLatitude(), m_setLocation.getLongitude());
				m_map.m_gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(m_setLocation.getLatitude(),m_setLocation.getLongitude()), 15));

			}
		}

    }

	public LocationManager getLocationManager() {
  	return (LocationManager)this.getSystemService(LOCATION_SERVICE);
  }
  public SensorManager getSensorManager() {
  	return (SensorManager)this.getSystemService(SENSOR_SERVICE);
  }
  public String getDeviceId() {
  	return Secure.getString(getContentResolver(), Secure.ANDROID_ID);
  }

	public RoadMap getMap() {
		int times = 0;
		try {
			while (m_map == null) {
				Thread.sleep(100);
				if (times++ > 250) {
					break;
					// raise exception to show we dont have map yet.
				}
			}
		} catch (Exception ex) {
			Log.d("getMap:",ex.getLocalizedMessage());
		}
		return m_map;
	}
  private void drawRoute(double lat1, double long1, double lat2, double long2) {
	    PolylineOptions line = new PolylineOptions();
	    line.width(5);
	    line.color(Color.RED);

        line.add(new LatLng(lat1,long1));
        line.add(new LatLng(lat2,long2));

	    getMap().m_gmap.addPolyline(line);
	}

	public int markJSONRecord(String rec) {

	     RoadSenseRecord rsRec = new RoadSenseRecord(rec);
	    
	     if (!rsRec.isJSON())
	    	 return 1;
	     
	     // Either we are on the first record in which case the first condition is true
	     // or we dont have a proper json record, then its an error case but just return for now.
	     if (rsRec.getVersion() != null || rsRec.getSpeed() == null)
	    	 return 2;

    //	 if (!rsRec.getLocationMethod().equalsIgnoreCase("1") || rsRec.getAccuracy() > 30)
  	//       return ;

    	 if (prevLat != 0) {
	    	   drawRoute(prevLat, prevLong, rsRec.getLatitude(), rsRec.getLongitude());
	    	   prevLat = rsRec.getLatitude();
	    	   prevLong = rsRec.getLongitude();
    	 } else {
    		  prevLat = rsRec.getLatitude();
	    	  prevLong = rsRec.getLongitude();
    	 }

	     if (!(rsRec.getType().equalsIgnoreCase(SensorBg.TYPE_AUTO) ||
	    	   rsRec.getType().equalsIgnoreCase(SensorBg.TYPE_MANUAL))) {
	    	
	    	 return 3;
	     }

	     boolean manualBump = false;


    	 String time = rsRec.getTime();
		 String type = rsRec.getType();
    	 String difT = rsRec.getAccDif();
    	 String speedT = rsRec.getSpeed();
    	 double speed = rsRec.getDoubleSpeed();
    	 double [] dif = rsRec.getAccDoubleDif(); 
 
    	if (speed > 25 || speed < 2) {
    		 return 4;
    	 }
    	 
    	 if (Math.abs(dif[0]) < 2 && Math.abs(dif[1]) < 2 && Math.abs(dif[2]) < 2)
    		 return 5;
    	 /* 	 
    	 if (speed > MARK_SPEED_LIMIT &&
    	    (!(difx == 0 && dify == 0 && difz == 0)))
    		 return;
    	 
    	 if (!(difx >= MARK_DIF_LIMIT ||
    		  dify >= MARK_DIF_LIMIT ||
    		  difz >= MARK_DIF_LIMIT))
    	 {
    		 if (!(difx == 0 && dify == 0 && difz == 0))
    			 return;
    		 else
    			 manualBump = true;
    	 }
    */

    // 	 timerText.setText(time);
    //	 speedText.setText(speedT);
    	 if (type.equalsIgnoreCase(SensorBg.TYPE_MANUAL))
    		 manualBump = true;

    	 
    	 getMap().addMarker(
        		  rsRec.getLatitude()+"",
        		  rsRec.getLongitude()+"",
        		  difT+"&"+speedT+"&"+time+"&"+type,
	              manualBump); 

    	 //m_map.moveCameratoLocation(rsRec.getLatitude(), rsRec.getLongitude());
         LatLng myLoc = new LatLng(rsRec.getLatitude(), rsRec.getLongitude());

         // Move the camera instantly to currentLoc with a zoom of 15.
         getMap().m_gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 15));


		return 0;
    }

  public void showSettingsAlert(){
  	if(true){
      AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
    
      // Setting Dialog Title
      alertDialog.setTitle("Message");
      
      // Setting Dialog Message
      alertDialog.setMessage("GPS is not enabled. Please enable it in Settings menu");

      // On pressing Settings button
      alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog,int which) {
              Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
              MapActivity.this.startActivity(intent);
          }
      });

      // on pressing cancel button
      alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
          dialog.cancel();
          }
      });

      // Showing Alert Message
      alertDialog.show();
  }
  }

  
 

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.map, menu);
    return true;
  }
  
 	class ClickEvent implements OnClickListener {
	    Button startButton = (Button)findViewById(R.id.start);
		public void onClick(View view) {
			if (view == startButton) {
				selectedDetection = DETECT_WHISTLE;
				recorderThread = new RecorderThread();
				recorderThread.start();
				detectorThread = new DetectorThread(recorderThread);
				detectorThread.setOnSignalsDetectedListener(MapActivity.mainApp);
				detectorThread.start();
				//goListeningView();
			}
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	
	@Override
	public void onWhistleDetected() {
		runOnUiThread(new Runnable() {
			public void run() {
		    	MediaPlayer mPlayer1 = MediaPlayer.create(MapActivity.this, R.raw.dingdong);
		    	mPlayer1.start();
			}
		});
	}
	
 
    
 // for uploading a file.
    private class UploadFile extends AsyncTask<Integer, Integer, Void> {

        //private final ProgressDialog dialog = new ProgressDialog(MyActivity.this);
        private File [] m_files;
        private int m_files_count;
       
        private boolean ret = false;
        
    	 public UploadFile(File [] files) {
    		 m_files = files;
    		 m_files_count = files.length;
    	 }
    	 
    protected void onPreExecute() {
     //
        super.onPreExecute();
        progressBar.setVisibility(View.VISIBLE);
        progressText.setVisibility(View.VISIBLE);
    }

    protected void onProgressUpdate(Integer... progress) {
        progressBar.setProgress((int) (progress[0]));
        progressText.setText(progress[0]+"/"+progressBar.getMax());
      }

    @Override
    protected Void doInBackground(Integer... params) {
    	 for (int i = 0 ; i < m_files_count; i++) {
    		 float percentage = ((float)i / (float)m_files_count) * 100;
    		 //ret = IOHttp.writeFileToHttp(m_files[i].getAbsolutePath());
    		 ret = IOHttp.zipFileAndWriteFileToHttp(m_files[i].getAbsolutePath());
    		 publishProgress( Float.valueOf(percentage).intValue()); 
    	 }
        return null;
    }

    protected void onPostExecute(Void result) {
    	progressBar.setVisibility( View.INVISIBLE);
        progressText.setVisibility(View.INVISIBLE);
     }
   }

	@Override
	public boolean onMarkerClick(Marker marker) {
		// TODO Auto-generated method stub
		String title = marker.getTitle();
		String [] ele= title.split("&");
		
		timerText.setText(ele[2]);
		speedText.setText(ele[1]);
		
		return false;
	}
} 
