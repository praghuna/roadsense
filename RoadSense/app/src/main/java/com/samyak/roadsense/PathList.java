package com.samyak.roadsense;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.samyak.roadsense.util.ReadPaths;

import org.json.JSONObject;

import java.util.ArrayList;

public class PathList extends Activity {
    ListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_list);

        final ListView lvPathList = (ListView)this.findViewById(R.id.pathList);
        ArrayList<JSONObject> result = ReadPaths.readPathFiles();

        adapter = new ListViewAdapter(this, R.layout.path_list_item, result, 0);
        // ListView lv
        // Msg = new ListView(this);
        lvPathList.setAdapter(adapter);


        lvPathList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position,
                                    long id) {

                Object o = lvPathList.getItemAtPosition(position);
                try {
                    Intent i = new Intent();
                    i.putExtra("fileName", adapter.getObjectAtPosition(position).getString("fileName"));
                    setResult(RESULT_OK, i);
                    finish();

                } catch (Exception ex) {
                    System.out.println("PathList:setOnItemClick:EXCEPTION:" + ex.getLocalizedMessage());
                }

            }
        });

    }
}
