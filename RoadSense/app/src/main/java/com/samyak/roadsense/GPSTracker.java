package com.samyak.roadsense;


import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.LightingColorFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.util.Log;


public class GPSTracker extends Service implements LocationListener {
 
    private Context mContext;
	LocationActivity m_locActivity;
    private LocationManager m_locManager;
    private int locationMethod = 0;
    // flag for GPS status
    public boolean isGPSEnabled = false;
 
    // flag for network status
    boolean isNetworkEnabled = false;
 
    // flag for GPS status
    boolean canGetLocation = false;
 
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
 
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
 
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
 
 
    public GPSTracker(Activity context) {
    	//this.m_rmap = map;
        LocationActivity loc = (LocationActivity)context;
    	this.m_locManager = loc.getLocationManager();
    	this.m_locActivity = loc;

    	getLocation();
    }

    
    public Location getLocation() {
    	if (location != null)
    		return location;


        try {
            // getting GPS status
             isGPSEnabled = m_locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // Here, thisActivity is the current activity
               // getting network status
            isNetworkEnabled = m_locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
 
            if (!isGPSEnabled && !isNetworkEnabled) {
                	locationMethod = 0;
            } else {
                this.canGetLocation = true;
 
                // first GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                	System.out.println("getting location from gps");
                    if (location == null) {

                    /*	m_locManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled"); */
                        if (m_locManager != null) {
                            location = m_locManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location == null) {
                                location = m_locManager
                                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            }

                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }

                        }
                    }
                    m_locActivity.useLocation(location);
                }
                else {
                	
                	// if gps is not enabled get location from Network Provider
                    if (isNetworkEnabled) {
                    	System.out.println("getting location from network");
                    	m_locManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("Network", "Network");
                        if (m_locManager != null) {
                            location = m_locManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                        m_locActivity.useLocation(location);
                    }
                }
                
                if(location==null)
                {
                	if (isNetworkEnabled) {
                    	System.out.println("getting location from network");

                        m_locManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("Network", "Network");
                        if (m_locManager != null) {
                            location = m_locManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
 
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        if (location != null) {
        	if (location.getProvider().equalsIgnoreCase(LocationManager.GPS_PROVIDER)) {
                locationMethod = 1;
            }
        	else if (location.getProvider().equalsIgnoreCase(LocationManager.NETWORK_PROVIDER)) {
        		locationMethod = 2;
        	}
        }
        return location;
    }
     

    public void startUsingGPS() {
    	m_locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
    	m_locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }
    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(m_locManager != null){
        	m_locManager.removeUpdates(GPSTracker.this);
        }       
    }
     
    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
         
        // return latitude
        return latitude;
    }
     
    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
         
        // return longitude
        return longitude;
    }
     
    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }
     
 
 /*   
    // addMarker provided the latlog is in a string format "lat,long"
    public void addMarker(String loc) {
    	StringTokenizer tokens = new StringTokenizer(loc, ",");
    	String sLat = tokens.nextToken();
    	String sLong = tokens.nextToken();
    	
        LatLng myLoc = new LatLng(Float.valueOf(sLat), Float.valueOf(sLong));

        m_rmap.m_gmap.addMarker(new MarkerOptions()
                            .position(myLoc)
                            .title(loc)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        
    }
*/
    // automatically called when os detects a location change.
    @Override
    public void onLocationChanged(Location location) {
    	this.location = location;

        if (location != null) {
        	if (location.getProvider().equalsIgnoreCase(LocationManager.GPS_PROVIDER))
        		locationMethod = 1;
        	else if (location.getProvider().equalsIgnoreCase(LocationManager.NETWORK_PROVIDER)) {
        		locationMethod = 2;
        	}
        }
 
         m_locActivity.useLocation(location);

  /*
         //this.clearAllMarkers();
   		if (this.location != null)
	   	  this.addMarker(this.location.getLatitude()+","+this.location.getLongitude());
*/
    }
    
    public int getLocationMethod() {
    	return locationMethod;
    }
 
    @Override
    public void onProviderDisabled(String provider) {
    }
 
    @Override
    public void onProviderEnabled(String provider) {
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        int a = 10;
    }
 
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}