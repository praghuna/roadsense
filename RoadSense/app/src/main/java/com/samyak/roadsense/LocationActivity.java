package com.samyak.roadsense;

import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;

public interface LocationActivity {
  public void useLocation(Location loc);
  public int markJSONRecord(String rec);
  public LocationManager getLocationManager();
  public SensorManager getSensorManager();
  public String getDeviceId();
}
