package com.samyak.roadsense.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

import com.samyak.roadsense.LocationActivity;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.R.attr.y;

public class IOHttp {
	
	  
	  static public int[] plotFileCoordinates_abs(String fileName, LocationActivity locA, boolean absPath) {
	      boolean zipFile = false;
		  int []recs = new int[2];
	      String orig_fileName = fileName;
		  int linecnt = 0;
		  int markedRecords=0;

		  if (fileName.endsWith("zip")) {
			  zipFile = true;
			  File location = null;
			  
			  if (!absPath) {
				  location = Environment.getExternalStorageDirectory();
			  } else {
				  File f = new File(fileName);
				  location = new File(f.getParent()+"/");
			  }
			//  fileName = fileName.replace("zip", "txt");
			  Decompress d = new Decompress(fileName, location.toString());
			  d.unzip();
  			  fileName = fileName.replace("zip", "txt");

		  }
		  
		  File sdcard;

		  //Get the text file
  		  File file ;

		  if (absPath) {
			  file = new File(fileName);
		  }
		  else {
			  sdcard = new File(Environment.getExternalStorageDirectory(), "bumps");
			  file = new File(sdcard,fileName);
		  }

        BufferedReader br;
		try {
		    br = new BufferedReader(new FileReader(file));
		    String line;
           Date prevTime = null;
		    while ((line = br.readLine()) != null) {
				linecnt++;
				// ignore comments
				if (line.startsWith("#"))
					continue;


				JSONObject point = new JSONObject(line);
				if (!point.has("DATE")) {
					continue;
				}

				DateFormat formatter = new SimpleDateFormat("dd:MM:yyyy:HH:mm:ss.SSS");
				Date currTime = formatter.parse(point.getString("DATE"));
				// plot points only that are atleast 3 secs apart. Otherwise there will be
				// huge number of points to be plotted.
				if (prevTime == null) {
					prevTime = currTime;
					continue;
				}
				if (currTime.getTime() - prevTime.getTime() < 3000) {
					continue;
				}
				prevTime = currTime;
				if (locA.markJSONRecord(line) == 0){
				   markedRecords++;
				}
		    }
		    
		    br.close();
		}
		catch (Exception e) {
			
			Log.e("plotFileCoordinates",e.getLocalizedMessage());
		    //You'll need to add proper error handling here
		}
		
		if (zipFile) {
			File f = new File(fileName);
			f.delete();
		}

		recs[0] = markedRecords;
		  recs[1] = linecnt;

		  return  recs;
	  }

	  
	  static public void plotFileCoordinates(String fileName, LocationActivity locA) {
         plotFileCoordinates_abs(fileName,locA,false);
	  }
	  
	  static public String readConfig(String configFile) {
		  String line = "";


		  BufferedReader br;
			try {
				File sdcard;

				//Get the text file
				File file ;
				sdcard = new File(Environment.getExternalStorageDirectory(), "bumps");
				file = new File(sdcard,configFile);

				br = new BufferedReader(new FileReader(file));
			    
	            boolean starting = true;
	           
			    line = br.readLine();
			    
			    br.close();
			}
			catch (IOException e) {
				
				Log.e("plotFileCoordinates",e.getLocalizedMessage());
			    //You'll need to add proper error handling here
			}
			return line;

	  }
	  static public void writeToFile(String fileName,String header,String sBody) throws IOException{
          BufferedWriter bW = null;

	        try
	        {
	            File root = new File(Environment.getExternalStorageDirectory(), "bumps");

	            if (!root.exists()) {
	                root.mkdirs();
	            }

	            File file = new File(root, fileName);

	            if (!file.exists()) {
	               //sBody = header+"\n"+sBody;
					sBody = header+sBody;
	            }
	            bW = new BufferedWriter(new FileWriter(file, true));
	            bW.write(sBody);
	            bW.newLine();
	            bW.flush();
	            bW.close();
	        }
	        catch(IOException e)
	        {
	             e.printStackTrace();
	             if (bW != null)
	            	 bW.close();
	        }
	       }

	static public void writeToFile(String fileName,String header,String [] records, int recordCount) throws IOException{
		BufferedWriter bW = null;

		try
		{
			File root = new File(Environment.getExternalStorageDirectory(), "bumps");

			if (!root.exists()) {
				root.mkdirs();
			}

			File file = new File(root, fileName);
			bW = new BufferedWriter(new FileWriter(file, true));

			if (!file.exists()) {
				//sBody = header+"\n"+sBody;
				bW.write(header);
				bW.newLine();
				bW.flush();
			}

			for (int i = 0 ; i < recordCount; i++) {

				bW.write(records[i]);
				bW.newLine();
				bW.flush();
			}
			bW.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
			if (bW != null)
				bW.close();
		}
	}

	static public void writeRecords(String fileName, String header, String sBody) {
		  try {
		    writeToFile(fileName,header,sBody);
		  } catch (Exception ex) {
			  Log.e("writeRecords",ex.getLocalizedMessage());
		  }
	  }
	  
	  static public String clearFile(String fileName, String uniq) {
	    	String dest = uniq;
	    	File sdcard = new File(Environment.getExternalStorageDirectory(), "bumps");

  		    File from = new File(sdcard,fileName);
	    	File to = new File(sdcard,dest);

		    try
	        {
		    	// generate a name based on current time
			    	// rename file to new file
	            from.renameTo(to);

	            Log.i("clearFile:src",from.toString());
	            Log.i("clearFile:dest",to.toString());
	        }
	        catch(Exception e)
	        {
	             e.printStackTrace();
	        }
		    
		    return to.getName();
	  }
	  

	  static public void postFile(String fileName) {
		  String url = "http://www.akash-vani.com/partha/upload.php";
		  File file = new File(fileName);
		  try {
		      HttpClient httpclient = new DefaultHttpClient();

		      HttpPost httppost = new HttpPost(url);

		      InputStreamEntity reqEntity = new InputStreamEntity(
		              new FileInputStream(file), -1);
		      reqEntity.setContentType("binary/octet-stream");
		      reqEntity.setChunked(true); // Send in multiple parts if needed
		      httppost.setEntity(reqEntity);
		      HttpResponse response = httpclient.execute(httppost);
		      //Do something with response...

		  } catch (Exception e) {
             Log.e("post", e.getMessage());
		  }
	  }


	  
		static public boolean zipFileAndWriteFileToHttp(String absoluteFileName) {
			
			   final HttpClient http_client = new DefaultHttpClient();
		   	   http_client.getParams().setParameter("http.socket.timeout", Integer.valueOf(90000)); // 90 seconds
		       String [] s = new String[1];
		        try 
		 	 	{
		 		
			      File extStore = Environment.getExternalStorageDirectory();
		 	 		
		 	 	  s[0] = absoluteFileName;
		 	 	  String zipAbsoluteFileName = absoluteFileName.replace("txt", "zip");
		 	 	  File fName = new File(zipAbsoluteFileName);
		 	 	  String filename = fName.getName();
				  String filepath = zipAbsoluteFileName;   
		 	 	  File file = new File(zipAbsoluteFileName);
		 	 	  Compress c = new Compress(s,zipAbsoluteFileName);   //first parameter is d files second parameter is zip file name
		          c.zip();    //call the zip function
		 	 	  URI uri = new URI("http://www.akash-vani.com/examples/servlets/servlet/UploadFile");
		  	      final HttpPost httppost = new HttpPost(uri);
		 

		                         FileEntity entity;
		                          if (filepath.substring(filepath.length()-3, filepath.length()).equalsIgnoreCase("txt") || 
		                                filepath.substring(filepath.length()-3, filepath.length()).equalsIgnoreCase("log")) 
		                          { 
		                             entity = new FileEntity(file,"text/plain; charset=\"UTF-8\""); 
		                             entity.setContentType("text/plain");
		                             entity.setChunked(true); 
		                          }
		                         else 
		                         { 
		                             entity = new FileEntity(file,"binary/octet-stream"); 
		                             entity.setChunked(true); 
		                         } 

		                         httppost.setEntity(entity); 
		                         //httppost.addHeader("FILENAME_STR", filename);
		                         httppost.addHeader("FILENAME_STR", filename);
		    			        HttpResponse response = null;
							try 
							{
								// make HTTP GET/POST call
								response = http_client.execute(httppost);
							} 
							catch (ClientProtocolException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} 
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
		  			        if (response != null) 
		  			        {
		  				          if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) 
			     			          { 
			                   		    return false;
			                 	      }
			     		  	         else
			        	     		  {
			     		  	        	 File origFile = new File(absoluteFileName);
			     		  	        	// if (!origFile.delete()) {
			     		  	        		 //TODO: take care of undeleted files. 
			     		  	        	// }
			     		  	        	 IOHttp.archiveFile(absoluteFileName);
	     		  	            	     IOHttp.archiveFile(zipAbsoluteFileName);

				                         return true; 
				                                      } 
		 		                      }
			     	       
		  	
		  	}
		 	catch (Exception e) 
		 	{
		       //TODO Exception
		 	}
			    
			   return false;
		 		
			}

	static public boolean writeFileToHttp(String fileName) {
			
	   final HttpClient http_client = new DefaultHttpClient();
   	   http_client.getParams().setParameter("http.socket.timeout", Integer.valueOf(90000)); // 90 seconds

	    try 
 	 	{
 		  File extStore = Environment.getExternalStorageDirectory();
 		  String mPath = extStore.getAbsolutePath();
		  String filepath = fileName;   
 	 	  File fName = new File(fileName);
 	 	  String filename = fName.getName();
 	 		File file = new File(filepath);
 	 		
 		  URI uri = new URI("http://www.akash-vani.com/examples/servlets/servlet/UploadFile");
  	      final HttpPost httppost = new HttpPost(uri);
 

                         FileEntity entity;
                          if (filepath.substring(filepath.length()-3, filepath.length()).equalsIgnoreCase("txt") || 
                                filepath.substring(filepath.length()-3, filepath.length()).equalsIgnoreCase("log")) 
                          { 
                             entity = new FileEntity(file,"text/plain; charset=\"UTF-8\""); 
                             entity.setContentType("text/plain");
                             entity.setChunked(true); 
                          }
                         else 
                         { 
                             entity = new FileEntity(file,"binary/octet-stream"); 
                             entity.setChunked(true); 
                         } 

                         httppost.setEntity(entity); 
                         httppost.addHeader("FILENAME_STR", filename);
         
    			        HttpResponse response = null;
					try 
					{
						// make HTTP GET/POST call
						response = http_client.execute(httppost);
					} 
					catch (ClientProtocolException e1) 
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} 
					catch (IOException e1) 
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
  			        if (response != null) 
  			        {
  				          if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) 
	     			          { 
	                   		    return false;
	                 	                  }
	     		  	         else
	        	     		         {
		                               return true; 
		                                      } 
 		                      }
	     	       
  	
  	}
 	catch (Exception e) 
 	{
       //TODO Exception
 	}
	    
	   return false;
 		
	}

	  
	static private void setMyContent(String runContent)
	{
		//content = runContent;
	}
	
	static public File[] listFilesInDir(String dirName) {
		String path = Environment.getExternalStorageDirectory().toString()+"/"+dirName;
		Log.d("Files", "Path: " + path);

        FilenameFilter fileNameFilter = new FilenameFilter() {
        	   
            @Override
            public boolean accept(File dir, String name) {
               if(name.lastIndexOf('.')>0)
               {
                  // get last index for '.' char
                  int lastIndex = name.lastIndexOf('.');
                  
                  // get extension
                  String str = name.substring(lastIndex);
                  
                  // match path name extension
                  if(str.equals(".txt"))
                  {
                     return true;
                  }
               }
               return false;
            }
         };
         // returns pathnames for files and directory

		File f = new File(path);        
		File file[] = f.listFiles(fileNameFilter);
		Log.d("Files", "Size: "+ file.length);
		for (int i=0; i < file.length; i++)
		{
		    Log.d("Files", "FileName:" + file[i].getName());
		}
		return file;
	}

	public boolean loadJSonData(Context context, String fileName){
	    boolean retVal = true;
	     
	    try {
	        InputStream input = context.openFileInput(fileName);
	        if (input != null) {
	             
	            InputStreamReader in = new InputStreamReader(input);
	            BufferedReader in2 = new BufferedReader(in);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = in2.readLine()) != null){
	                try {
	                    // fill data from json object
	                    JSONObject json = new JSONObject(line);

	                     
	         
	                    } catch (JSONException je) {
	                         
	                        Log.e("json error reading data", je.toString());
	                                            retVal = false;
	                    }

	            	sb.append(line);
	                 
	            }
	            in2.close();
	            in.close();
	            
	 
	             
	              
	        } // if (input != null)
	        else
	            retVal = false;
	         
	    } catch (IOException ie) {
	        Log.e("File reading error", ie.toString());
	        retVal = false;
	    }
	     
	     
	     
	    return retVal;
	}

    static  public boolean archiveFile(String fileName) {
	     File destDir = new File(Environment.getExternalStorageDirectory(), "bumps/archive");
	     File srcDir =  new File(Environment.getExternalStorageDirectory(), "bumps");
	     String fn = new File(fileName).getName(); 
	     File sourceFile = new File(srcDir, fn);
	     File destFile = new File(destDir,fn);
	     
	     if (!destDir.exists()) {
	         destDir.mkdirs();
	     }
	     
	     boolean succ = sourceFile.renameTo(destFile);
	     
	     if (succ)
	    	 return true;
	     else
           return false;	     

	 }
}


 class ConnectionDetector {

/*** Function to check either mobile or wifi network is available or not. ***/

   public static boolean networkStatus(Context context) {

          return (ConnectionDetector.isWifiAvailable(context) || ConnectionDetector.isMobileNetworkAvailable(context));

   }

   public static boolean isMobileNetworkAvailable(Context ctx) {

      ConnectivityManager connecManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

      NetworkInfo myNetworkInfo = connecManager.getActiveNetworkInfo();

      if (myNetworkInfo.isConnected()) {
                 return true;
      } else {
                 return false;
      }
   }

   public static boolean isWifiAvailable(Context ctx) {

      ConnectivityManager myConnManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo myNetworkInfo = myConnManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
      if (myNetworkInfo.isConnected())
                    return true;
      else
                    return false;
    }
    
 
  }

