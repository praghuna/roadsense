package com.samyak.roadsense;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.support.v4.app.NavUtils;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.Chronometer.OnChronometerTickListener;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;
import com.samyak.roadsense.util.IOHttp;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RecordActivity extends Activity implements  LocationActivity, OnMarkerClickListener, OnMapReadyCallback{
	private	  Button manualRecord ;
 	private	  Button manualHide ;
    private   RoadMap m_map;
 	private   CheckBox autoDingDong;
	private   int m_stopType = 0;
	static private int RECORD_STOP = 1;
	static private int RECORD_PAUSE = 2;


 	private String g_file;
 	MediaPlayer mPlayerDingdong ;
    SensorBg m_sensor;
	private long m_startTime = 0;
	private TextView timerText= null;
	private TextView speedText = null; 
	private Button stopButton = null;
	private long startTime = 0;
    Chronometer stopWatch = null;
    private long countUp=0;
	private  TextView currentLoc;
    double prevLat = 0;
    double prevLong = 0;
	private TextView coordinates ;
	private TextView magnetometer ;
	private TextView gyrometer ;
	private TextView acclerometer;
	private TextView totalRecords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
		Intent parentIntent = getIntent();
		/*
          int mapId = R.id.rmap;

          FragmentManager fm  = (FragmentManager) this.getFragmentManager();
          MapFragment mf = (MapFragment)fm.findFragmentById(mapId);
		mf.getMapAsync(this);
*/
        Bundle extras = parentIntent.getExtras();        
        g_file = extras != null ? extras.getString("g_file") : "";
        
		  // archive the existing file
    	  String uniq = new SimpleDateFormat("ddMMHHmmss").format(new Date())+
    			       Secure.getString(getContentResolver(), Secure.ANDROID_ID)+ ".txt";

		  IOHttp.clearFile(g_file,uniq);

		// load all the buttons and other widets
    	manualRecord = (Button) findViewById(R.id.manualRecord);
     	manualHide = (Button) findViewById(R.id.manualHide);
        autoDingDong = (CheckBox) findViewById(R.id.autoDingDong);
        timerText = (TextView) findViewById(R.id.timer);
        speedText = (TextView) findViewById(R.id.currentspeed);

        stopWatch = (Chronometer) findViewById(R.id.chrono);
    	stopButton = (Button)findViewById(R.id.stop);
    	currentLoc = (TextView) findViewById(R.id.currentLoc);

		 coordinates = (TextView)findViewById(R.id.locCoordinates);
		 magnetometer = (TextView)findViewById(R.id.magnetoMeter);
		 gyrometer = (TextView)findViewById(R.id.gyro);
		 acclerometer = (TextView)findViewById(R.id.accelerometer);
		totalRecords = (TextView)findViewById(R.id.recordsTotal);

		autoDingDong.setVisibility(View.VISIBLE);

        // show/hide Manual record button on pressing manual hide
	    manualHide.setOnClickListener(new OnClickListener(){
        
     	    	public void onClick (View arg0) {
      	    		if (manualRecord.isShown())
     	    			manualRecord.setVisibility(View.INVISIBLE);
     	    		else
     	    			manualRecord.setVisibility(View.VISIBLE);
     	    	}
     	    });
     	    
        // on manualRecord write the current parameters to the file.
        manualRecord.setOnClickListener(new OnClickListener() {
        	public void onClick(View arg0) {
    			m_sensor.writeRecords(m_sensor.getCurrentRecord(SensorBg.TYPE_MANUAL),true);

    			playDingDong(true);
    			
    	    //	mPlayer1.release();
        	}
        });

     	stopWatch.setOnChronometerTickListener(new OnChronometerTickListener() {
     	      	
     	          public void onChronometerTick(Chronometer arg0) {
     	              countUp = (SystemClock.elapsedRealtime() - startTime) / 1000;
     	              String min = (countUp/60) < 10 ? "0"+(countUp/60) : (countUp/60)+"";
     	              String sec = (countUp%60) < 10 ? "0"+(countUp%60) : (countUp%60)+"";
     	              
     	              String asText = min + ":" + sec; 
     	              timerText.setText(asText);
     	              int int_speed = m_sensor.getSpeed();
     	              //speedText.setText(int_speed+" kmph");
					  speedText.setText(int_speed+"");
					  coordinates.setText(m_sensor.getCoordinates());
                      magnetometer.setText(m_sensor.getMagnetometer());
                      gyrometer.setText(m_sensor.getGyrometer());
                      acclerometer.setText(m_sensor.getAccelerometer());
					  totalRecords.setText(m_sensor.getTotalRecords());

     	          }
     	 });

        stopButton.setOnClickListener (new OnClickListener() {
	    	  
	    	 public void onClick(View arg0) {
  		      startTime = SystemClock.elapsedRealtime();
  		      stopWatch.stop();
				 m_stopType = RECORD_STOP;

  		      if (m_sensor != null) {
		        m_sensor.unregister();
  		        m_sensor = null;
  		      }

 		      setResult(Activity.RESULT_OK);

  		      finish();
		     }
	    });

     	timerText.setText("00:00");
        startTime = SystemClock.elapsedRealtime();
        stopWatch.start();
//		stopButton.setBackground(getResources().getDrawable(R.drawable.rec));

        // Show the Up buttongps in the action bar.
      //  setupActionBar();
    }

	public void onMapReady(GoogleMap map) {

		m_map = new RoadMap(map,"small_blue_dot.jpg");

		m_map.m_gmap.setOnMarkerClickListener((OnMarkerClickListener) this);
		// clear the markers from map
		m_map.clearAllMarkers();

		currentLoc.setOnClickListener(new OnClickListener() {
			// position map to current location.
			public void onClick(View arg0) {
				m_map.moveCameratoLocation(m_sensor.getLatitude(), m_sensor.getLongitude());
				LatLng myLoc = new LatLng(m_sensor.getLatitude(), m_sensor.getLongitude());

				// Move the camera instantly to currentLoc with a zoom of 15.
				m_map.m_gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 15));
 			}

		});
		LatLng myLoc = new LatLng(m_sensor.getLatitude(), m_sensor.getLongitude());

		// Move the camera instantly to currentLoc with a zoom of 15.
		m_map.m_gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 15));

	}
    public void playDingDong(boolean force) {

   	 if (!autoDingDong.isChecked() && !force)
   		 return;
   	 
        if (mPlayerDingdong == null)
     	  mPlayerDingdong = MediaPlayer.create(RecordActivity.this, R.raw.dingdong);
     
   	 mPlayerDingdong.start();
     }
    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    protected void onStart() {
    	super.onStart();

		if (m_sensor == null) {
			m_sensor = new SensorBg(this, g_file, getString(R.string.config_filename));
			m_sensor.register();
		}

		if (m_map == null) {
			return;
		}


     	if (m_sensor.isGPSEnabled() == false) {
  			  setResult(Activity.RESULT_CANCELED);
  			  finish();
  			  return;
  		}
        LatLng myLoc = new LatLng(m_sensor.getLatitude(), m_sensor.getLongitude());

        // Move the camera instantly to currentLoc with a zoom of 15.
        m_map.m_gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 15));

        // Zoom in, animating the camera.
        m_map.m_gmap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

    protected void onStop() {
    	super.onStop();

		if ((m_stopType == RECORD_STOP || m_stopType == RECORD_PAUSE) && m_sensor != null) {
			m_sensor.unregister();
			m_sensor = null;
		}
	}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.record, menu);
        return true;
    }
    

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    // change the lable of the currentLoc button when the location method
    // changes. This will be called by GPSTracker
    public void useLocation(Location loc) {
  	
    String locationMethod = loc.getProvider();
    
  	  if (locationMethod.equalsIgnoreCase(LocationManager.GPS_PROVIDER)) {
  		  currentLoc.setText("GPS");
  	  } else if (locationMethod.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER)) {
  		  currentLoc.setText("NET");
  	  } else
  		  currentLoc.setText("NO_LOC");

		/*
      LatLng myLoc = new LatLng(loc.getLatitude(), loc.getLongitude());

      // Move the camera instantly to currentLoc with a zoom of 15.
     m_map.m_gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 15));
     */
   	 if (prevLat != 0) {
    	   //drawRoute(prevLat, prevLong, loc.getLatitude(), loc.getLongitude());
    	   prevLat = loc.getLatitude();
    	   prevLong = loc.getLongitude();
	 } else {
		  prevLat = loc.getLatitude();
    	  prevLong = loc.getLongitude();
	 }

    }
    
    public LocationManager getLocationManager() {
    	return (LocationManager)this.getSystemService(LOCATION_SERVICE);
    }
    
    public SensorManager getSensorManager() {
    	return (SensorManager)this.getSystemService(SENSOR_SERVICE);
    }
 
    public String getDeviceId() {
    	return Secure.getString(getContentResolver(), Secure.ANDROID_ID);
    }
 
    private void drawRoute(double lat1, double long1, double lat2, double long2) {
	    PolylineOptions line = new PolylineOptions();
	    line.width(5);
	    line.color(Color.RED);

        line.add(new LatLng(lat1,long1));
        line.add(new LatLng(lat2,long2));

	    m_map.m_gmap.addPolyline(line);
	}
 
    public int markJSONRecord(String rec) {
 	    // ignore comments 
	    if (rec.startsWith("#"))
	    	 return 1;
	    
	     RoadSenseRecord rsRec = new RoadSenseRecord(rec);
	    
	     // Either we are on the first record in which case the first condition is true
	     // or we dont have a proper json record, then its an error case but just return for now.
	     if (rsRec.getVersion() != null || rsRec.getSpeed() == null)
	    	 return 2;
 
	     if (!rsRec.getLocationMethod().equalsIgnoreCase("1") || rsRec.getAccuracy() > 100)
    	       return 3 ;
	     
	     boolean manualBump = false;
	     
    	 //m_map.moveCameratoLocation(rsRec.getLatitude(), rsRec.getLongitude());

    	 if (rsRec.getType().equalsIgnoreCase(SensorBg.TYPE_MANUAL))
    		 manualBump = true;
    	 
    	 m_map.addMarker(
        		  rsRec.getLatitude()+"",
        		  rsRec.getLongitude()+"",
        		  "0",
	              manualBump);
		return 0;
 
    }
    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
         public void onReceive(final Context context, final Intent intent) {
            RecordActivity.this.onMessageReceived();
         }
    };
    
    @Override
    protected void onResume() {
    	super.onResume();
        registerReceiver(myReceiver, new IntentFilter("com.samyak.roadsense.ACTION"));
/*
		if (m_sensor != null) {
			m_sensor.register();
		}
		*/
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(new CustomPhoneStateListener(m_sensor,g_file), PhoneStateListener.LISTEN_CALL_STATE);

    }

    @Override
    protected void onPause() {
    	super.onPause();

		if (m_stopType == RECORD_STOP) {
			unregisterReceiver(myReceiver);
			m_stopType = RECORD_PAUSE;
		}

		/*
		if (m_sensor != null) {
			m_sensor.unregister();
		}*/
        
    }

    private void onMessageReceived() {
    	
    }

	@Override
	public boolean onMarkerClick(Marker marker) {
		// TODO Auto-generated method stub
		String title = marker.getTitle();
		String [] ele= title.split("|");
		return false;
	}

}

class CustomPhoneStateListener extends PhoneStateListener {

    //private static final String TAG = "PhoneStateChanged";
     
    SensorBg m_sensor;
    String m_fileName;
    public CustomPhoneStateListener(SensorBg sensor, String fileName) {
        super();
        
        m_sensor = sensor;
        m_fileName = fileName;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);
        String type = null;
        switch (state) {
        case TelephonyManager.CALL_STATE_IDLE:
            //when Idle i.e no call
        	type = SensorBg.TYPE_PHONE_IDLE;
            break;
        case TelephonyManager.CALL_STATE_OFFHOOK:
            //when Off hook i.e in call
            //Make intent and start your service here
            type = SensorBg.TYPE_PHONE_OFFHOOK;
        	break;
        case TelephonyManager.CALL_STATE_RINGING:
            //when Ringing
        	type = SensorBg.TYPE_PHONE_RINGING;
            break;
            
        default:
            break;
        }
        if (type == null)
        	return;
        
     	m_sensor.writeRecords(m_sensor.getCurrentRecord(type),true);

    }
}

