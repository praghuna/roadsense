package com.samyak.roadsense.util;

import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

import static android.R.id.input;

/**
 * Created by parthasarathyraghunathan on 12/07/16.
 */
public class ReadPaths {

    static public ArrayList<JSONObject> readPathFiles() {
        String path = Environment.getExternalStorageDirectory().toString() + "/bumps";
        File f = new File(path);
        File files[] = f.listFiles();

        ArrayList<JSONObject> result = new ArrayList<>();
        Arrays.sort( files, new Comparator()
        {
            public int compare(Object o1, Object o2) {

                if (((File)o1).lastModified() > ((File)o2).lastModified()) {
                    return -1;
                } else if (((File)o1).lastModified() < ((File)o2).lastModified()) {
                    return +1;
                } else {
                    return 0;
                }
            }

        });
        // read each file and get the details for puttingin the path_list_item
        for (int i = 0; i < files.length; i++) {
            String startTime = null;
            String endTime = null;
            JSONObject json;
            RandomAccessFile input = null;
            String line = null;
            String lastLine = null;

            int linecnt = 0;
            if (files[i].toString().endsWith(".txt") == false) {
                continue;
            }
            try {
                try {
                    input = new RandomAccessFile(files[i], "r");
                    long fileLength = input.length();
                    // BufferedReader input = new BufferedReader(new FileReader(files[i]));

/*
                    line = input.readLine();
                    linecnt++;
                    // first line is just a status line. Ignore it for now.
                    json = new JSONObject(line);

                    // second line has actual data.
                    line = input.readLine();
                    linecnt++;
                    if (line != null) {
                        json = new JSONObject(line);
                        if (json.has("DATE")) {
                            startTime = json.getString("DATE");
                            endTime =  json.getString("DATE");
                        }
                    }
*/
                    // keep reading till the end to get the endTime.
                    while ((line = input.readLine()) != null) {
                        linecnt++;
                        lastLine = line;
                        if (startTime == null) {
                            json = new JSONObject(line);
                            if (json.has("DATE")) {
                                startTime = json.getString("DATE");
                                if (fileLength > 10000) {
                                    input.seek(input.length() - 2000);
                                    continue;
                                }
                            }
                        }
                    }

                    if (lastLine != null) {
                            json = new JSONObject(lastLine);
                            if (json.has("DATE")) {
                                endTime = json.getString("DATE");
                            } else {
                                endTime = startTime; // ERROR actually. Every record should have date.
                                continue; // skip this file as we dont have end time.
                            }
                    }


                    if (startTime == null || endTime == null) {
                        continue;
                    }

                    Log.d("Readpaths:file", files[i].getName() + ":" + linecnt);
                    DateFormat formatter = new SimpleDateFormat("dd:MM:yyyy:HH:mm:ss.SSS");
                    DateFormat outputFmt = new SimpleDateFormat("dd:MM:yyyy HH:mm:ss");
                    Date date1 = formatter.parse(startTime);
                    Date date2 = formatter.parse(endTime);

                    // Get msec from each, and subtract.
                    long diff = date2.getTime() - date1.getTime();

                    // we dont want to show paths that are less than a minute duration.
                    if (diff > 60000 /*&& diff > 36000*/) {
                        JSONObject res = new JSONObject();
                        res.put("startTime", "Start: " + outputFmt.format(date1));
                        res.put("endTime", "End: " + outputFmt.format(date2));
                        res.put("fileName", files[i]);

                        Double dur = (double) diff / (1000 * 60);
                        Log.d("Duration=", dur.toString());
                        res.put("duration", dur.longValue() + " min");
                        Log.d("Duration long val=", dur.longValue() + "");
                        result.add(res);
                    }

                } catch (Exception ex) {
                    //do nothing. It will close and move to next file.
                    ex.printStackTrace();
                } finally {
                    input.close();
                }
            }catch (Exception ex1) {
                Log.d("IOException:",ex1.getLocalizedMessage());
            }
            // the format should be same as what is present in SensorBg.java

        }
        return result;
    }

}
