package com.samyak.roadsense;


import android.os.Looper;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class RoadMap  {

	   GoogleMap m_gmap;
	   String m_markerPng;
	   String m_startTime ;
	   public RoadMap(GoogleMap map, String markerPng) {
		   m_gmap  = map;
		   m_markerPng = markerPng;
	   }
	    // addMarker provided the lat and long separately with the given title.
	    public void addMarker(String sLat, String sLong, String title, boolean manualBump) {
	    	
	        LatLng myLoc = new LatLng(Float.valueOf(sLat), Float.valueOf(sLong));

			// dont attempt to update UI when running in background.
			if (!Looper.getMainLooper().equals(Looper.myLooper())) {
				return;
			}

	        if (manualBump)
	           m_gmap.addMarker(new MarkerOptions()
	                          .position(myLoc)
	                          .title(title)
	                          .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
	        else
	        	/*
	            m_gmap.addMarker(new MarkerOptions()
	              .position(myLoc)
	              .title(title)
	               .icon(BitmapDescriptorFactory.fromAsset("small_blue_dot.jpg")));
	              */
				m_gmap.addMarker(new MarkerOptions()
						.position(myLoc)
						.title(title)
						.icon(BitmapDescriptorFactory.fromAsset(m_markerPng)));


 		}
	    
	    // removes all the markers from the map
	    public void clearAllMarkers() {
	    	m_gmap.clear();
	    }
	 
	    
		public void moveCameratoLocation(double lat, double lon) {
	        LatLng myLoc = new LatLng(lat, lon);

	        // Move the camera given location with current zoom level.
	        m_gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc,m_gmap.getCameraPosition().zoom));
	  	
	    }
		


}
