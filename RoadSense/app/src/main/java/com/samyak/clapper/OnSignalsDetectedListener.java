package com.samyak.clapper;

public interface OnSignalsDetectedListener{
	public abstract void onWhistleDetected();
}