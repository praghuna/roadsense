package com.samyak.unimap.core;

import android.app.Activity;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by parthasarathyraghunathan on 05/11/15.
 */
public class Common {


    static public String loadFileFromAsset(Activity ctx,String fileName) {
        String json = null;
        try {

            InputStream is = ctx.getAssets().open(fileName);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }


}
