package com.samyak.unimap.core;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by parthasarathyraghunathan on 16/10/15.
 */
public class Indexer {
    String mFileName;
    Context mContext;
    HashMap<String,Boolean> mMatras;
    HashMap<String,Boolean> mOnlyFirst;
    HashMap<String, Boolean> mWords;
    HashMap<String, ArrayList<WordMap>> mSubWords;
    HashMap<String, ArrayList<String>> mIndexByStarting;

    String mFileContents;
    ArrayList<String> mLineContents;

    public Indexer (String fileContents, HashMap<String, Boolean>matras, HashMap<String, Boolean>only_first) {
        mFileContents = fileContents;
        mLineContents = new ArrayList<>();
        mMatras = matras;
        mOnlyFirst = only_first;
        mIndexByStarting = new HashMap<>();
        mSubWords = new HashMap<>();
        mWords = new HashMap<>();
    }

    public HashMap<String,ArrayList<WordMap>> SplitToLines() {

        //String [] lines = mFileContents.split("\\n");
        String [] lines = mFileContents.split("<p>");
        int lineno = 0;
        for(String line: lines) {
            String str = lines[lineno];
            //int pos = matcher.start();
            //str = mFileContents.substring(matcher.start(), matcher.end());
            mLineContents.add(str);
            SplitToWords(lineno, str);
            lineno++;
        }
        return mSubWords;
    }

    public ArrayList<String> getLines() {
        return mLineContents;
    }

    public HashMap<String,ArrayList<WordMap>> SplitToWords(int lineno, String line) {
        Matcher matcher = Pattern.compile("\\S+").matcher(line);

        while (matcher.find()) {
            String str = matcher.group();
            int pos = matcher.start();

            ArrayList<String> subWords = getSubStr(str);
            int charPos = 0;
            for (String sw: subWords) {
                ArrayList<WordMap> subwords = mSubWords.get(sw);
                boolean newWord = true;

                if (subwords == null) {
                    subwords = new ArrayList<>();
                } else {
                    newWord = false;
                }
                subwords.add(new WordMap(sw, lineno, pos+charPos++));
                // if the word was already seen dont bother to index it by its first char
                // it would have been done already.
                if (newWord) {
                    // hash this word in each of the char that appears in the word.
                    ArrayList<String> chars = getChars(sw);
                    for (int i = 0; i < chars.size(); i++) {
                        ArrayList<String> strList = mIndexByStarting.get(chars.get(i));

                        if (strList == null) {
                            strList = new ArrayList<>();
                        }
                        strList.add(sw);

                        mIndexByStarting.put(chars.get(i), strList);
                    }
                }
                mSubWords.put(sw,subwords);
            }
            //System.out.println(matcher.start() + ":" + matcher.group());
        }
        return mSubWords;
    }

    public ArrayList<String> getChars(String str) {
        int offset =  0, strLen = str.length();
        String sub = null;
        int prevCharPos = 0;
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < strLen; i++) {
            String matraChar = null;

            if (i < strLen - 1)
              matraChar = str.substring(i+1, i+2);

            Boolean val = mMatras.get(matraChar);
            if (val != null && val) {
                continue;
            }

            sub = str.substring(prevCharPos,i+1);
            prevCharPos = i+1;

            list.add(sub);
        }

        return list;

    }

    public String getStartingChar(String str) {
        int offset =  0, strLen = str.length();
        String sub = null;
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < strLen; i++) {
            String singleChar = str.substring(i, i + 1);

            Boolean val = mMatras.get(singleChar);
            if (val != null && val) {
                continue;
            }
            String sstr = str.substring(i).substring(0, 1);

            sub = str.substring(0,i);

            // we only want the first char. The loop is for including the
            // matras. Break after we got to the end of first char.
            break;
        }

        return sub;

    }


    /*
    public HashMap<String, WordMap> Index() {

        mWords = new HashMap<>();
        Matcher matcher = Pattern.compile("\\S+").matcher(mFileContents);

        while (matcher.find()) {
            String str = matcher.group();
            int pos = matcher.start();
            ArrayList<Integer> list = mWords.get(str);
            if (list == null) {
                list = new ArrayList<>();
            }

            list.add(pos);
            mWords.put(str, list);
            //System.out.println(matcher.start() + ":" + matcher.group());
        }

        deriveSortedSubWords();
        return mSubWords;
    }

*/
    public ArrayList<String> getSubStr(String str) {
        int offset = 0, strLen = str.length();
        ArrayList<String> subWords = new ArrayList<>();

        for (int i = 0; i < strLen; i++) {
            String singleChar = str.substring(i, i + 1);

            Boolean val = mMatras.get(singleChar);
            if (val != null && val) {
                continue;
            }
            String sstr = str.substring(i).substring(0, 1);
            if (sstr.equals("|") || sstr.equals(":") ||
                    sstr.equals("\n") || sstr.equals("-"))
                continue;

            String sub = str.substring(i);
            subWords.add(sub);
        }
        return subWords;
    }


      public void deriveSortedSubWords() {
        for (String key: mWords.keySet()) {
            getSubStr(key);
        }
    }

    public ArrayList<String> wordListWithChar(String langChar) {
        ArrayList<String> list = mIndexByStarting.get(langChar);

        if (list == null)
            return null;
        else
            return list;
    }

    public HashMap<String, ArrayList<WordMap>> findInitialWords(String subword) {
        ExecutionTime.StartEvent("findInitialWords:"+subword);

        HashMap<String, ArrayList<WordMap>> matches = null;

      //  String startChar = getStartingChar(subword);
        ArrayList<String> chars = getChars(subword);
        ArrayList<String> finalList = null;
        for (int i = 0 ; i < chars.size(); i++) {
            ArrayList<String> list = mIndexByStarting.get(chars.get(i));

            if (list == null || list.size() == 0) {
                continue;
            }
            if (finalList == null || finalList.size() > list.size()) {
                finalList = list;
            }
        }

        if (finalList != null) {
            matches = listToHash(finalList);
        }
        ExecutionTime.EndEvent();
        return matches;
    }

    public HashMap<String, ArrayList<WordMap>> listToHash (ArrayList<String> list) {
        HashMap<String, ArrayList<WordMap>> matches = new HashMap<>();

        int listlen = list.size();
        for (int i = 0; i < listlen; i++) {
            String word = list.get(i);

            if (matches.get(word) != null)
                continue;

            ArrayList<WordMap> match = mSubWords.get(word);

            matches.put(word, match);
        }

        return matches;
    }

    public HashMap<String, ArrayList<WordMap>> findSubWords(String subword, HashMap<String, ArrayList<WordMap>> subwords) {
        ExecutionTime.StartEvent("findSubWords:"+subword+":"+subwords.size());
        System.out.print("findSubWords:subword:" + subword);

        HashMap<String, ArrayList<WordMap>> matches = new HashMap<> ();

        //     int index = Collections.binarySearch(subWords,new WordMap(subword,subword),mComparePrefix);
        int index = 0;
        if (index != -1) {
            for (String key: subwords.keySet()) {
                boolean prefix = key.startsWith(subword);
                if (prefix) {
                    matches.put(key, subwords.get(key));
                }
            }
        }
        ExecutionTime.EndEvent();
        System.out.println(":match_count:"+matches.size());
        return matches;
    }

}
