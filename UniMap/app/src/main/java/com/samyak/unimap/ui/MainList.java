package com.samyak.unimap.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.samyak.unimap.R;
import com.samyak.unimap.core.Common;
import com.samyak.unimap.core.MainListAdapter;
import com.samyak.unimap.core.listSounds;


public class MainList extends Activity {

    ListView mMainList;
    ListAdapter mAdapter;
    String mFileContents;
    listSounds mLangSounds;
    private void init() {
        mFileContents = Common.loadFileFromAsset(this,"vsn_tamil.txt");

        mLangSounds= new listSounds(this, mFileContents);
        mLangSounds.computeMap();
        mLangSounds.reverseMap();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);

        init();
        mMainList = (ListView) findViewById(R.id.mainList);
        mAdapter = new MainListAdapter(this, R.layout.mainlist_item,mLangSounds.getLines() );
        mMainList.setAdapter(mAdapter);
/*
        mMainList.setOnTouchListener(new DoubleClickListener() {

            @Override
            public void onSingleClick(View v) {

            }

            @Override
            public void onDoubleClick(View v) {
                int len = mMainList.getAdapter().getCount();

                for (int i = 0; i < len; i++) {
                    View itemView = mMainList.getChildAt(i);
                    TextView tv = (TextView)itemView.findViewById(R.id.lineText);
                    float ht = tv.getTextSize()*(float)1.001;
                    tv.setTextSize(ht);
                    v.setMinimumHeight(tv.getMaxHeight());
                }
            }
        });
        */

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

abstract class DoubleClickListener implements ListView.OnTouchListener {

    private static final long DOUBLE_CLICK_TIME_DELTA = 300;//milliseconds

    long lastClickTime = 0;

    @Override
    public boolean onTouch(View v, MotionEvent ev) {
        long clickTime = System.currentTimeMillis();
        if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA && ev.getAction() == MotionEvent.ACTION_UP){
            onDoubleClick(v);
        } else if (ev.getAction() == MotionEvent.ACTION_UP){
            onSingleClick(v);
        }
        lastClickTime = clickTime;
        return true;
    }

    public abstract void onSingleClick(View v);
    public abstract void onDoubleClick(View v);
}