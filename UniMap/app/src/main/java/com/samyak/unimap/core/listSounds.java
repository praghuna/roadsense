package com.samyak.unimap.core;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by parthasarathyraghunathan on 05/10/15.
 */
public class listSounds {
    Context mContext;
    HashMap<String,ArrayList<String>> mSoundMap;
    HashMap<String,ArrayList<String>> mReverseSoundMap;
    HashMap<String,ArrayList<String>> mBeginSoundMap;
    //ComparePrefix mComparePrefix ;

    HashMap<String, Object> mCache;
    Indexer mIndex;
    HashMap<String,Boolean> mMatras;
    HashMap<String,Boolean> mOnlyFirst;
    int mMaxSoundLen = 0;
    String mFileContents;
    int mNumSubWords;
    HashMap<String, ArrayList<WordMap>> mSubWords;
    public listSounds(Context ctx, String fileContents) {
        mContext = ctx;
        mSoundMap = new HashMap<String,ArrayList<String>>();
        mReverseSoundMap = new HashMap<String,ArrayList<String>>();
        mBeginSoundMap = new HashMap<String,ArrayList<String>>();
        mMatras = new HashMap<String, Boolean>();
        mOnlyFirst = new HashMap<String, Boolean>();
        mCache = new HashMap<>();
        mFileContents = fileContents;
        mNumSubWords = mFileContents.length();
    }

    public ArrayList<String> getLines() {
        return mIndex.getLines();
    }
    private ArrayList<String> expand(String sound) {
        ArrayList<String> vals = new ArrayList<String>();
        vals.add(sound);

        for(int i = 0 ; i < vals.size(); ) {
            String val = vals.get(i);

            if (val.contains("%")) {
                int varStartPos = val.indexOf("%{") + 2;
                int varEndPos = val.indexOf('}', varStartPos);
                String varName = val.substring(varStartPos, varEndPos);
                ArrayList<String> varSounds = mSoundMap.get(varName);

                for (int j = 0; j < varSounds.size(); j++) {
                    String newVal = new String(val);
                    newVal = newVal.replace("%{" + varName + "}", varSounds.get(j));

                    vals.add(newVal);
                }
                vals.remove(i);
                i = 0;
            } else {
                i++;
            }

        }

        return vals;
    }


    public void computeMap() {
        ExecutionTime.StartEvent("computeMap");

        ExecutionTime.StartEvent("loadTamilJson");
        String json = loadFileFromAsset("tamil.json");
        ExecutionTime.EndEvent();

        try {
            JSONObject soundsJSON = new JSONObject(json);
            JSONArray allArray = soundsJSON.getJSONArray("all");
            JSONArray matras = soundsJSON.getJSONArray("matras");
            JSONArray only_first = soundsJSON.getJSONArray("only_first");

            for(int j = 0 ; j < matras.length(); j++) {
                mMatras.put(matras.getString(j),true);
            }
            for(int j = 0 ; j < only_first.length(); j++) {
                mOnlyFirst.put(only_first.getString(j),true);
            }

            for(int i = 0 ; i < allArray.length(); i++ ) {
                JSONObject map = allArray.getJSONObject(i);
                JSONArray soundsArray = map.getJSONArray("sounds");
                ArrayList<String> sounds = new ArrayList<String>();

                for(int j = 0 ; j < soundsArray.length(); j++) {
                    sounds.addAll(expand(soundsArray.getString(j)));
                }

                mSoundMap.put(map.getString("word"), sounds);

             //   System.out.println(map.getString("alias") + "->" + map.getString("word"));
            }
        }catch(Exception ex) {
            System.out.println("computeMap:EXCEPTION:"+ex.getLocalizedMessage());
        }
       // deriveSortedSubWords();
        //mComparePrefix = new ComparePrefix(mMatras);
        ExecutionTime.StartEvent("Indexer");
        mIndex = new Indexer(mFileContents, mMatras, mOnlyFirst);
        mSubWords = mIndex.SplitToLines();
        ExecutionTime.EndEvent();

        ExecutionTime.EndEvent();
    }


     public void reverseMap() {

        for(String key : mSoundMap.keySet()) {
            ArrayList<String> sounds = mSoundMap.get(key);

            for(int i = 0; i < sounds.size(); i++) {
                String sound = sounds.get(i);
                String beginSound = sound.substring(0,1);
                ArrayList<String> reverseSounds = mReverseSoundMap.get(sound);
                ArrayList<String> beginSounds = mBeginSoundMap.get(beginSound);

                if (sound.length() > mMaxSoundLen) {
                    mMaxSoundLen = sound.length();
                }

                if (reverseSounds == null) {
                    reverseSounds = new ArrayList<String>();
                }

                if (beginSounds == null) {
                    beginSounds = new ArrayList<String>();
                }
                reverseSounds.add(key);
                if (!beginSounds.contains(sound)) {
                    beginSounds.add(sound);
                }
                mReverseSoundMap.put(sound,reverseSounds);
                mBeginSoundMap.put(beginSound, beginSounds);
            }
        }
    }


    public HashMap<String, ArrayList<WordMap>>  listWords(String trans) {
        HashMap<String,ArrayList<WordMap>> obj = (HashMap<String,ArrayList<WordMap>>)mCache.get(trans);
        if (obj != null)
            return obj;

        String prefix = trans.substring(0,trans.length()-1);
        obj = (HashMap<String,ArrayList<WordMap>>)mCache.get(prefix);

        if (obj != null) {
            obj = listWords("", trans, obj, 1);
        } else {
            obj = listWords("", trans, mSubWords, 1);
        }
        mCache.put(trans,obj);

        return obj;
    }

    public HashMap<String, ArrayList<WordMap>> listWords(String prefix, String trans, HashMap<String, ArrayList<WordMap>> subwords, int level) {
        HashMap<String, ArrayList<WordMap>>  langWords = new HashMap<> ();

      //  String spaces = String.format("%"+level+"s", " ");
     //   System.out.println(spaces+":"+prefix+":"+trans+":"+subwords.size());

        if (subwords.size() == 0 ) {
           subwords = mSubWords;
        }

        HashMap<String, ArrayList<WordMap>>  matches = null;
            if (prefix != null && prefix.length() > 0) {
                HashMap<String,ArrayList<WordMap>> newWords = null;

                if (subwords.size() == mSubWords.size())
                    newWords = mIndex.findInitialWords(prefix);

                if (newWords != null && newWords.size() < subwords.size()) {
                    subwords = newWords;
                }
                matches = mIndex.findSubWords(prefix, subwords);
                if (matches == null || matches.size() == 0)
                    return null;
            }

        if (trans == null || trans.length() == 0) {
           return matches;
        }

        int bounds = Math.min(mMaxSoundLen,trans.length());

        for(int i = 0; i < bounds; i++) {
            // get prefix of the trans and check if it matches any chars
            // if it matches then prefix the match with what ever is found
            // for the rest of the trans recursively.
            String begin = trans.substring(0,i+1);

            HashMap<String, Boolean> langChars = new HashMap<>();


            // if this is the last phrase for a char match then add
            // all those possible chars that could be derived by
            // adding some additional transliterate chars.
            if (i+1 == bounds) {
                ArrayList<String> sounds ;
                sounds = mBeginSoundMap.get(begin.substring(0,1));

                for (String sound : sounds) {
                    if (sound.startsWith(begin)) {
                        ArrayList<String> chars = mReverseSoundMap.get(sound);
                        if (chars != null) {
                            for (String chr : chars)
                                langChars.put(chr, true);
                        }
                    }
                }
            } else {
                ArrayList<String> chars = mReverseSoundMap.get(begin);
                if (chars != null) {
                    for (String chr : chars)
                        langChars.put(chr, true);
                }
            }

            // if there is no mapping for a  prefix then we wont need to
            // go any further.
            if (langChars == null || langChars.size() == 0)
                continue;

            String suffix = trans.substring(i + 1);

            for (String currChar : langChars.keySet()) {

                if (prefix != null && prefix.length() > 0 && mOnlyFirst.get(currChar) != null  )
                    continue;

                ArrayList<String> list = mIndex.wordListWithChar(currChar);
                if (list == null || list.size() == 0 ) {
                    continue;
                }
                HashMap<String, ArrayList<WordMap>>  filteredSubWords = listWords(prefix+currChar,
                                                                       suffix, matches == null ? subwords : (matches.size() > list.size()) ? mIndex.listToHash(list):matches,
                                                             level +1);

                if ((filteredSubWords== null || filteredSubWords.size() == 0)) {
                    continue;
                }

                for( String key: filteredSubWords.keySet()) {
                    ArrayList<WordMap> wm = langWords.get(key);
                    if (wm == null) {
                        wm = new ArrayList<>();
                    }
                    wm.addAll(filteredSubWords.get(key));
                    langWords.put(key, wm);
                }

                // if there is a word that matches every input then no need to
                // check for other possible strings as it is only going to produce utmost
                // a subset or equivalent set of matches.
                if ((matches != null && filteredSubWords.size() == matches.size()) ||
                        (subwords.size() == filteredSubWords.size()))
                    break;
            }
        }

        return langWords;
    }

    private String loadFileFromAsset(String fileName) {
        String json = null;
        try {

            InputStream is = mContext.getAssets().open(fileName);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

}

