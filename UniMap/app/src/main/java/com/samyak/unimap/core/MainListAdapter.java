package com.samyak.unimap.core;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.samyak.unimap.R;

import java.util.ArrayList;

/**
 * Created by parthasarathyraghunathan on 05/11/15.
 */
public class MainListAdapter extends BaseAdapter implements Filterable {
    Context mContext;
    ArrayList<String> mLines;
    int mLayoutResourceId;
    public MainListAdapter(Context context, int layoutResourceId, ArrayList<String> lines) {
        mContext = context;
        mLines = lines;
        mLayoutResourceId = layoutResourceId;

    }

    @Override
    public int getCount() {
        if(mLines != null) {
            return mLines.size();
        } else {
            return 0;
        }

    }

    @Override
    public Object getItem(int i) {
        if (mLines != null) {
            return mLines.get(i);
        } else {
            return "";
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            view = inflater.inflate(mLayoutResourceId, viewGroup, false);
        }

        TextView tv = (TextView) view.findViewById(R.id.lineText);

        int lines = mLines.get(i).split("\n").length;
        tv.setText(mLines.get(i));

        tv.setHeight(tv.getLineHeight()*lines);

        return view;
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}
