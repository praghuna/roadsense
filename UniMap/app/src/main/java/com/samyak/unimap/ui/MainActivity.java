package com.samyak.unimap.ui;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.BackgroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.samyak.unimap.R;
import com.samyak.unimap.core.Common;
import com.samyak.unimap.core.ExecutionTime;
import com.samyak.unimap.core.listSounds;
import com.samyak.unimap.core.WordMap;
import com.samyak.unimap.ui.AutoResizeTextView;
import android.os.Handler;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends Activity {
    EditText mInputText ;
    AutoResizeTextView mResultText;
    HashMap<String, ArrayList<WordMap>> mLangWords;
    listSounds mLangSounds;
    String mFileContents;
    boolean mScrolling = false;
    AsyncTaskRunner mScroller ;
    long lastTimeTextChanged = 0;
    Runnable mRunnable;
    Handler mHandler;
    int mScrollDistance = 3;
    SeekBar mScrollSpeed;

    private void init() {
        mFileContents = Common.loadFileFromAsset(this,"vsn_tamil.txt");

        mLangSounds= new listSounds(this, mFileContents);
        mLangSounds.computeMap();
        mLangSounds.reverseMap();

    }
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        mHandler=new Handler();

        mRunnable=new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                mInputText.setVisibility(View.INVISIBLE); //If you want just hide the View. But it will retain space occupied by the View.
          //      yourLayoutObject.setVisibility(View.GONE); //This will remove the View. and free s the space occupied by the View
            }
        };
        mHandler.postDelayed(mRunnable, 5000);

        mScrollSpeed = (SeekBar)findViewById(R.id.seekBar);

        mScrollSpeed.setMax(20);

        mScrollSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                mScrollDistance = progresValue;
             //   Toast.makeText(getApplicationContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
             //   Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //textView.setText("Covered: " + progress + "/" + seekBar.getMax());
              //  Toast.makeText(getApplicationContext(), "Stopped tracking seekbar", Toast.LENGTH_SHORT).show();
            }
        });


    //  mLangSounds.dumpMap();
     /*   mLangWords = mLangSounds.listWords("saarathy");

        for(int i = 0 ; i < mLangWords.size(); i++) {
            System.out.println("Word:"+ mLangWords.get(i));
        }
*/
       // mLangSounds.DumpSubStr();
        mResultText = (AutoResizeTextView) findViewById(R.id.resultText);
        mResultText.setText(mFileContents);

        mResultText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (mScrolling) {
                    mScroller.cancel(true);
                    mScrolling = false;
                } else {
                    mScroller = new AsyncTaskRunner();
                    mScroller.execute();
                    mScrolling = true;
                }
                toggleControls();
                return false;
            }
        });

        mInputText= (EditText) findViewById(R.id.editText);
        mInputText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                long startTime = System.nanoTime();
                String searchStr = cs.toString();
                /*
                long currentTime = System.currentTimeMillis();
                // dont search again if the typing is still happening or we are typing after
                // a long gap.
                if (currentTime - lastTimeTextChanged < 1000 || currentTime - lastTimeTextChanged > 10000) {
                    lastTimeTextChanged = System.currentTimeMillis();
                    return;
                }
                lastTimeTextChanged =  currentTime;
                // dont bother to search for single characters.
*/
                if (searchStr.length() < 2)
                    return;
                SpannableStringBuilder spanText = new SpannableStringBuilder(mFileContents);

                ExecutionTime.clear();
                mLangWords = mLangSounds.listWords(searchStr);
           //     mResultText.setText("");
                int firstWordPos = -1;
                if (mLangWords != null) {
                    ArrayList <WordMap> tempArr = new ArrayList<WordMap>();
                    for (String key: mLangWords.keySet()) {
                        if (mLangWords.get(key) != null)
                            tempArr.addAll(mLangWords.get(key));
                    }

                    for (WordMap temp: tempArr) {
                       int firstPos = temp.pos().get(0);

                            spanText.setSpan(new BackgroundColorSpan(0xFFFFFF00), firstPos, firstPos + temp.word().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            System.out.println("FINAL:"+temp.word());

//                        mResultText.append("\n" + temp.fullWord() + " "+ temp.posString());

                    }

                    mResultText.setText(spanText);
   //                 mResultText.bringPointIntoView(firstWordPos);
                    //int y = mResultText.getLayout().getLineForOffset(firstWordPos);
                    //mResultText.scrollTo(0,y);
                }
      //          ExecutionTime.listDuration();
                startTime = System.nanoTime() - startTime;
                System.out.println("TIME:"+startTime);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    private void toggleControls() {
        if (mInputText.getVisibility() == View.INVISIBLE)
          mInputText.setVisibility(View.VISIBLE);
        else
            mInputText.setVisibility(View.INVISIBLE);

        mHandler.postDelayed(mRunnable, 8000);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;

        @Override
        protected String doInBackground(String... params) {
            // publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                while(true) {
                   mResultText.scrollBy(0,mScrollDistance);
                    Thread.sleep(300);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resp;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(String... text) {
            // finalResult.setText(text[0]);
            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }

}
