package com.samyak.unimap.core;

import java.util.ArrayList;

/**
 * Created by parthasarathyraghunathan on 13/10/15.
 */
public class WordMap {
    String mSubWord;
   // String mFullWord;
    ArrayList<Integer> mPos;
    ArrayList<Integer> mLineno;

    public WordMap(String subword, int lineno, int pos) {
      //  mFullWord = fullWord;
        mSubWord = subword;
        mPos = new ArrayList<>();
        mLineno = new ArrayList<>();
        mPos.add(pos);
        mLineno.add(lineno);
    }

    public void addPos(int lineno, int pos) {
        mPos.add(pos);
        mLineno.add(lineno);
    }
 /*
    public WordMap matchPrefix(String prefix) {
        int index = -1;
        if (mSubWord.startsWith(prefix)) {
            return this;
        } else {
            return null;
        }
    }
*/
    public WordMap match(String substr) {

        if (mSubWord.startsWith(substr)) {
            return this;
        } else {
            return null;
        }
    }


   // public String fullWord() {        return mFullWord;    }

    public String word() { return mSubWord;}
    public ArrayList<Integer> pos() { return mPos; }

    public String posString() {
        String pos = "" ;
        for(int i = 0 ; i < mPos.size(); i++) {
            pos = pos + "," + mPos.get(i);
        }
        return pos;
    }
}
