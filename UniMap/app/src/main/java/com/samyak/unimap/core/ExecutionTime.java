package com.samyak.unimap.core;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by parthasarathyraghunathan on 28/10/15.
 */
public class ExecutionTime {
    static private ArrayList<String> mEventNames = new ArrayList<>();
    static private ArrayList<Long> mDuration = new ArrayList<>();
    static private Stack<String> mEventStack = new Stack<>() ;
    static private Stack<Long> mStartTime = new Stack<>();

    static public void StartEvent(String evtName) {
        mEventStack.push(evtName);
        mStartTime.push(System.nanoTime());
    }

    static public void EndEvent() {
        mEventNames.add(mEventStack.pop());
        long startTime = mStartTime.pop();
        mDuration.add(System.nanoTime() - startTime);
       // listDuration();
    }

    static public void listDuration() {
        for(int i = 0; i < mEventNames.size(); i++) {
            System.out.println("ExecutionTime:"+mEventNames.get(i)+ "->"+mDuration.get(i) );
        }
        System.out.println("---------------------" );
    }

    static public void clear() {
        mEventNames.clear();
        mDuration.clear();
        mEventStack.clear();
        mStartTime.clear();
    }

}
