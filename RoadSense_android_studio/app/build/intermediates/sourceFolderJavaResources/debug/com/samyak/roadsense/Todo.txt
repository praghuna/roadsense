- Add distance covered by day/trip/total
- Add time switched on recording
- provision for adding email and sync it with email id in trip
  web login using email should show all and only those belonging to the email
  (change record version when adding email)

- automatically list the available trips for choosing when doing a marking
- record call details and sms details and other notification details
- make it a service and attachable through front end ui
- check the battery usage and resource usage when running for long duration
- add bell indicator on detecting a bump
- add config receiving from server
- take care of exceptions and add tracing 
- dont do rotation even if auto rotate

- map path of travel - done
- On tapping a marker show the details in the windows widgets - done
- upload indicator should show bytes/files uploaded and bytes/files remaining - done
- add zip compatability to readers - done


- use different image for different types of markers


bugs
 - sometimes getSpeed() crashes after stopping recording
 - the gps is still used after stopping recording
 - zip produces zero zip file this could potentially lose data
 - zip files dont seem to be read into the db or they are read but not moved to archive location in web
   the results in duplicate entries in db. 