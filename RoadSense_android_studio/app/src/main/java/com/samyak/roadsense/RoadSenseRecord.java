package com.samyak.roadsense;

import java.text.DecimalFormat;

import org.json.JSONObject;

public class RoadSenseRecord {

	JSONObject m_rec;
	
	public RoadSenseRecord(String rec) {
		try {
		m_rec = new JSONObject(rec);
		} catch (Exception ex) {
			// TODO handle exception;
		}
	}
	
	public boolean isJSON() {
		return m_rec != null;
	}
	
    private String getValue(String key) {
    	String val = null;
    	try {
    	  val = m_rec.getString(key);
    	} catch (Exception ex) {
    		// TODO: Handle exception
    	}
    	return val;
    }
    
    public String getTime() {
    	String dt = getValue(SensorBg.DATE);
    	String [] tm = dt.split(":");
    	return gt(tm,3)+":"+gt(tm,4)+":"+gt(tm,5);
    }
    
    public String getLocationMethod() {
    	return getValue(SensorBg.LOCATION_METHOD);
    }
    public String getSpeed() {
    	return getValue(SensorBg.SPEED) + " kmph";
    }
    public double getDoubleSpeed() {
    	return Double.parseDouble(getValue(SensorBg.SPEED));
    }

    public String getType() {
    	return getValue(SensorBg.TYPE);
    }
    
    public double getLatitude() {
    	return Double.parseDouble(getValue(SensorBg.LATITUDE));
    }

    public double getLongitude() {
    	return Double.parseDouble(getValue(SensorBg.LONGITUDE));
    }
    
    public int getAccuracy() {
    	return Integer.parseInt(getValue(SensorBg.ACCURACY));
    }

    public double[] getAccDoubleDif() {
    	String oldVal = getValue(SensorBg.ACCELROMETER_OLD);
    	String newVal = getValue(SensorBg.ACCELEROMETER_NEW);

    	String [] oldarr = oldVal.split(",");
    	String [] newarr = newVal.split(",");
    	
    	double [] dif = new double[3];
    	dif[0] = Float.parseFloat(newarr[1]) - Float.parseFloat(oldarr[1]);
    	dif[1] = Float.parseFloat(newarr[2]) - Float.parseFloat(oldarr[2]);
    	dif[2] = Float.parseFloat(newarr[3]) - Float.parseFloat(oldarr[3]);

    	return dif;
    }
    public String getAccDif() {
		DecimalFormat df2=new DecimalFormat("0.00");

    	double [] dif = getAccDoubleDif();
    	String val = df2.format(dif[0])+","+df2.format(dif[1])+","+df2.format(dif[2]);
    	
    	return val;
    }
    
    public String getVersion() {
    	return getValue(SensorBg.VERSION);
    }
	private String gt(String [] tokens, int index) {
		if (tokens.length > index)
			return tokens[index];
		else
			return "";
	}
}
