package com.samyak.roadsense;




import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.os.Build.VERSION;
import android.os.Build;
import android.content.pm.PackageManager;




import android.graphics.Color;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;
import com.samyak.roadsense.util.IOHttp;
import com.samyak.clapper.*;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

@SuppressLint({ "NewApi", "SimpleDateFormat" })
public class MapActivity extends Activity  implements OnSignalsDetectedListener, LocationActivity, OnMarkerClickListener, OnMapReadyCallback {
  static final String FILE = "http://m.uploadedit.com/b026/1387987285344.txt";
  String g_file ;
  static final String g_file1 = "coordinates.txt";
  private String g_openFile;
  RoadMap m_map;
	public static final int DETECT_NONE = 0;
	public static final int DETECT_WHISTLE = 1;
	public static final int RECORD_ACTIVITY_INTENT = 2;
	public static int selectedDetection = DETECT_NONE;

	// detection parameters
	private DetectorThread detectorThread;
	private RecorderThread recorderThread;

	static MapActivity mainApp;

	private TextView timerText= null;
	private TextView speedText = null;
	private TextView progressText = null;
	private	Button startButton;
	private	Button chooseFile ;
    private Button resetButton;
    private Button uploadButton;
    private ProgressBar progressBar;
    
    double prevLat = 0;
    double prevLong = 0;

    // invoked after an activity started by this activity has ended. Currently
    /// the only activity started by this activity is ACTION_GET_CONTENT (see below)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    
     switch(requestCode){
     case 1:
      // if a file is selected plot it on the map. The sanity of the data in the
      // file is not checked. IMPORTANT !! Please select only valid files.
  	  if(resultCode==RESULT_OK){
        String FilePath = data.getData().getPath();
        g_openFile = FilePath;
        IOHttp.plotFileCoordinates_abs(g_openFile, this ,true);
      }
      break;
     case RECORD_ACTIVITY_INTENT:
    	 if (resultCode == RESULT_CANCELED) {
    		   runOnUiThread( new Runnable() {
 	    	     public void run() {
    		        showSettingsAlert();
    		     }
    		   });
    	 }
     }
   }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    
    mainApp = this;
    
    setContentView(R.layout.activity_map);
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);

  MapFragment mapFragment = (MapFragment) getFragmentManager()
			  .findFragmentById(R.id.map);
	  mapFragment.getMapAsync(this);



	  startButton = (Button)findViewById(R.id.start);
	chooseFile = (Button) findViewById(R.id.chooseFile);
    resetButton = (Button)findViewById(R.id.reset);
    uploadButton = (Button) findViewById(R.id.upload);
    
    timerText = (TextView) findViewById(R.id.timer);
    speedText = (TextView) findViewById(R.id.currentspeed);
    progressBar = (ProgressBar) findViewById(R.id.uploading);
    progressText = (TextView) findViewById(R.id.progressText);
 

    
    g_file = getString(R.string.bumps_filename);
    
    uploadButton.setOnClickListener(new OnClickListener() {
    	public void onClick(View arg0) {
    	  String uniq = new SimpleDateFormat("ddMMHHmmss").format(new Date())+
	    			       Secure.getString(getContentResolver(), Secure.ANDROID_ID)+ ".txt";


    		IOHttp.clearFile(g_file, uniq);
    		UploadFile uf  = new UploadFile(IOHttp.listFilesInDir("bumps"));
            uf.execute((Integer)null);
    	}
    });
    
 
    // setup click listeners for the buttons
    startButton.setOnClickListener (new OnClickListener() {
    	 public void onClick(View arg0) {
   			  Intent myIntent = new Intent(MapActivity.this, RecordActivity.class);
   			  myIntent.putExtra("g_file", g_file.toString()); //Optional parameters
   	          startActivityForResult(myIntent, RECORD_ACTIVITY_INTENT);

//    		  MapActivity.this.startActivity(myIntent);
     	  }
    });
    
    // open file browser and use the file selected.
    // the plotting of the file will be done by onActivityResult() above.
    chooseFile.setOnClickListener(new OnClickListener() {
    	public void onClick(View arg0) {
			boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
			if (isKitKat) {
				Intent intent = new Intent();
				intent.setType("*/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(intent,0);

			} else {
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("*/*");
				startActivityForResult(intent,0);
			}


			//Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);

    	 // fileIntent.setType("file/*"); // intent type to filter application based on your requirement
    	 // startActivityForResult(fileIntent,1 );
     	}
    });
    
  }
  
  public void useLocation(Location loc) {
  	
  }


	public void onMapReady(GoogleMap map) {
		m_map = new RoadMap(map);
		m_map.m_gmap.setOnMarkerClickListener(this);

		resetButton.setOnClickListener(new OnClickListener(){

										   public void onClick(View arg0) {
											   try {
												   m_map.clearAllMarkers();
											   } catch(Exception exp) {

											   }
										   }
									   }
		);
	}

	public LocationManager getLocationManager() {
  	return (LocationManager)this.getSystemService(LOCATION_SERVICE);
  }
  public SensorManager getSensorManager() {
  	return (SensorManager)this.getSystemService(SENSOR_SERVICE);
  }
  public String getDeviceId() {
  	return Secure.getString(getContentResolver(), Secure.ANDROID_ID);
  }

  private void drawRoute(double lat1, double long1, double lat2, double long2) {
	    PolylineOptions line = new PolylineOptions();
	    line.width(5);
	    line.color(Color.RED);

        line.add(new LatLng(lat1,long1));
        line.add(new LatLng(lat2,long2));

	    m_map.m_gmap.addPolyline(line);
	}

	public void markJSONRecord(String rec) {
 	    // ignore comments 
	    if (rec.startsWith("#"))
	    	 return;
	    
	     RoadSenseRecord rsRec = new RoadSenseRecord(rec);
	    
	     if (!rsRec.isJSON())
	    	 return;
	     
	     // Either we are on the first record in which case the first condition is true
	     // or we dont have a proper json record, then its an error case but just return for now.
	     if (rsRec.getVersion() != null || rsRec.getSpeed() == null)
	    	 return;

    	 if (!rsRec.getLocationMethod().equalsIgnoreCase("1") || rsRec.getAccuracy() > 30)
  	       return ;

    	 if (prevLat != 0) {
	    	   drawRoute(prevLat, prevLong, rsRec.getLatitude(), rsRec.getLongitude());
	    	   prevLat = rsRec.getLatitude();
	    	   prevLong = rsRec.getLongitude();
    	 } else {
    		  prevLat = rsRec.getLatitude();
	    	  prevLong = rsRec.getLongitude();
    	 }

	     if (!(rsRec.getType().equalsIgnoreCase(SensorBg.TYPE_AUTO) ||
	    	   rsRec.getType().equalsIgnoreCase(SensorBg.TYPE_MANUAL))) {
	    	
	    	 return;
	     }

	     boolean manualBump = false;
	     
    	 //m_map.moveCameratoLocation(rsRec.getLatitude(), rsRec.getLongitude());
         LatLng myLoc = new LatLng(rsRec.getLatitude(), rsRec.getLongitude());

         // Move the camera instantly to currentLoc with a zoom of 15.
         m_map.m_gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 15));

    	 
    	 String time = rsRec.getTime();
    	 String difT = rsRec.getAccDif();
    	 String speedT = rsRec.getSpeed();
    	 double speed = rsRec.getDoubleSpeed();
    	 double [] dif = rsRec.getAccDoubleDif(); 
 
    	 if (speed > 20 || speed < 2) {
    		 return;
    	 }
    	 
    	 if (Math.abs(dif[0]) < 3 && Math.abs(dif[1]) < 3 && Math.abs(dif[2]) < 3)
    		 return;
    	 /* 	 
    	 if (speed > MARK_SPEED_LIMIT &&
    	    (!(difx == 0 && dify == 0 && difz == 0)))
    		 return;
    	 
    	 if (!(difx >= MARK_DIF_LIMIT ||
    		  dify >= MARK_DIF_LIMIT ||
    		  difz >= MARK_DIF_LIMIT))
    	 {
    		 if (!(difx == 0 && dify == 0 && difz == 0))
    			 return;
    		 else
    			 manualBump = true;
    	 }
    */

    	 timerText.setText(time);
    	 speedText.setText(speedT);
    	 if (rsRec.getType().equalsIgnoreCase(SensorBg.TYPE_MANUAL))
    		 manualBump = true;
    	 
    	 
    	 m_map.addMarker(
        		  rsRec.getLatitude()+"",
        		  rsRec.getLongitude()+"",
        		  difT+"&"+speedT+"&"+time,
	              manualBump); 
 
    }

  public void showSettingsAlert(){
  	if(true){
      AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
    
      // Setting Dialog Title
      alertDialog.setTitle("Message");
      
      // Setting Dialog Message
      alertDialog.setMessage("GPS is not enabled. Please enable it in Settings menu");

      // On pressing Settings button
      alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog,int which) {
              Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
              MapActivity.this.startActivity(intent);
          }
      });

      // on pressing cancel button
      alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
          dialog.cancel();
          }
      });

      // Showing Alert Message
      alertDialog.show();
  }
  }

  
 

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.map, menu);
    return true;
  }
  
 	class ClickEvent implements OnClickListener {
	    Button startButton = (Button)findViewById(R.id.start);
		public void onClick(View view) {
			if (view == startButton) {
				selectedDetection = DETECT_WHISTLE;
				recorderThread = new RecorderThread();
				recorderThread.start();
				detectorThread = new DetectorThread(recorderThread);
				detectorThread.setOnSignalsDetectedListener(MapActivity.mainApp);
				detectorThread.start();
				//goListeningView();
			}
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	
	@Override
	public void onWhistleDetected() {
		runOnUiThread(new Runnable() {
			public void run() {
		    	MediaPlayer mPlayer1 = MediaPlayer.create(MapActivity.this, R.raw.dingdong);
		    	mPlayer1.start();
			}
		});
	}
	
 
    
 // for uploading a file.
    private class UploadFile extends AsyncTask<Integer, Integer, Void> {

        //private final ProgressDialog dialog = new ProgressDialog(MyActivity.this);
        private File [] m_files;
        private int m_files_count;
       
        private boolean ret = false;
        
    	 public UploadFile(File [] files) {
    		 m_files = files;
    		 m_files_count = files.length;
    	 }
    	 
    protected void onPreExecute() {
     //
        super.onPreExecute();
        progressBar.setVisibility(View.VISIBLE);
        progressText.setVisibility(View.VISIBLE);
    }

    protected void onProgressUpdate(Integer... progress) {
        progressBar.setProgress((int) (progress[0]));
        progressText.setText(progress[0]+"/"+progressBar.getMax());
      }

    @Override
    protected Void doInBackground(Integer... params) {
    	 for (int i = 0 ; i < m_files_count; i++) {
    		 float percentage = ((float)i / (float)m_files_count) * 100;
    		 //ret = IOHttp.writeFileToHttp(m_files[i].getAbsolutePath());
    		 ret = IOHttp.zipFileAndWriteFileToHttp(m_files[i].getAbsolutePath());
    		 publishProgress( Float.valueOf(percentage).intValue()); 
    	 }
        return null;
    }

    protected void onPostExecute(Void result) {
    	progressBar.setVisibility( View.INVISIBLE);
        progressText.setVisibility(View.INVISIBLE);
     }
   }

	@Override
	public boolean onMarkerClick(Marker marker) {
		// TODO Auto-generated method stub
		String title = marker.getTitle();
		String [] ele= title.split("&");
		
		timerText.setText(ele[2]);
		speedText.setText(ele[1]);
		
		return false;
	}
} 
