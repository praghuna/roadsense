package com.samyak.roadsense;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class ReceiverHandler extends BroadcastReceiver{

	   @Override
	   public void onReceive(Context context, Intent intent) {

	        Intent it = new Intent();
	        it.setAction("com.samyak.roadsense.ACTION");

	        Log.d("broadcastreceiver"," called");
	        // Rebroadcasts to your own receiver. 
	        // This receiver is not exported; it'll only be received if the receiver is currently registered.
	        context.sendBroadcast(it); 
	   }

}