package com.samyak.roadsense;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.util.Log;
import com.samyak.roadsense.util.IOHttp;

public class SensorBg implements SensorEventListener{
	private SensorManager mSensorManager;
	
	static final public String LATITUDE = "LATITUDE";
	static final public String LONGITUDE = "LONGITUDE";
	static final public String SPEED = "SPEED";
	static final public String BEFORE_SPEED = "BEFORE_SPEED";
	static final public String LOCATION_METHOD = "LOCATION_METHOD";
	static final public String ACCURACY = "ACCURACY";
	static final public String ACCELEROMETER_NEW = "ACCELEROMETER_NEW";
	static final public String ACCELROMETER_OLD = "ACCELROMETER_OLD";
	static final public String MAGNETIC_NEW = "MAGNETIC_NEW";
	static final public String ROTATION_MATRIX = "ROTATION_MATRIX";
	static final public String IDENT_MATRIX = "IDENT_MATRIX";
	static final public String ORIENTATION = "ORIENTATION";
	static final public String INCLINATION = "INCLINATION";
	static final public String BEARING = "BEARING";
	static final public String DATE = "DATE";
	static final public String TYPE = "TYPE";
	static final public String VERSION = "VERSION";
	static final public String DEVICE_NAME = "DEVICE_NAME";
	static final public String DEVICE_ID = "DEVICE_ID";

	static final private int RECORD_FORMAT_VERSION = 4;
	static final public String TYPE_AUTO = "AUTO";
	static final public String TYPE_MANUAL = "MANUAL";
	static final public String TYPE_HEARTBEAT = "HEARTBEAT";
	static final public String TYPE_PHONE_IDLE = "PHONE_IDLE";
	static final public String TYPE_PHONE_RINGING = "PHONE_RINGING";
	static final public String TYPE_PHONE_OFFHOOK = "PHONE_OFFHOOK";


	static final private int LIMIT = 3;
	  private double MARK_DIF_LIMIT = 4.5;
	  private double MARK_SPEED_LIMIT = 6.0;
	// previous sensor values
	private static float prevx = 0, prevy = 0, prevz = 0;
	// accelerometer sensor
	private Sensor mAcc = null;
	private Sensor mMag = null;
	
	// GPS
	public GPSTracker m_gps;
	
	// main activity
	private LocationActivity m_locActivity;
	
	private String g_fileName ;
	private String g_configFile;
	private String g_header;
    public  mySensor mSense;
    private myTimer mTask;
    public Timer timer;
    int delay = 10000;  
    private String m_header = null;
 
	public SensorBg(LocationActivity locActivity, String fileName, String config) {
		    m_locActivity = locActivity;
	        m_gps =  new GPSTracker(m_locActivity);
	        
	        g_fileName = fileName;
	        g_configFile = config;
	        
	        String conf = IOHttp.readConfig(g_configFile);
	        
	        String tokens[] = conf.split(",");
	        if (tokens.length >= 2)
	        {
	           MARK_DIF_LIMIT = Double.parseDouble(tokens[0]);
	           MARK_SPEED_LIMIT = Double.parseDouble(tokens[1]);
	        }
	        g_header = this.getJSONHeader();
			mSensorManager = m_locActivity.getSensorManager();
            mSense = new mySensor(20);
            timer =new Timer();
            mTask= new myTimer(g_fileName, this);
            m_header = null;
            m_header = getJSONHeader();
         //   timer.scheduleAtFixedRate(mTask, delay, delay);
	}

	public void register() {
		if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
			  // Success! There's an accelerometer
			  mAcc =    mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		      mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_NORMAL);
			}
			else {
			  // Failure! No accelerometer.
		    }

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null){
			  // Success! There's an accelerometer
			  mMag =    mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		      mSensorManager.registerListener(this, mMag, SensorManager.SENSOR_DELAY_NORMAL);
			}
			else {
			  // Failure! No magnetometer.
		    }
        timer =new Timer();
        mTask= new myTimer(g_fileName, this);

        timer.scheduleAtFixedRate(mTask, delay, delay);
        
        if (m_gps != null) {
        	m_gps.startUsingGPS();
        }
	}

	public void unregister() {
		mSensorManager.unregisterListener(this);
		if (timer != null) {
			timer.cancel();
		}
		timer = null;
		
		if (m_gps != null) {
			m_gps.stopUsingGPS();
			m_gps = null;
		}
	}
	
	public int getSpeed() {
		Double t = m_gps != null ? m_gps.getLocation() != null ? m_gps.getLocation().getSpeed()*3.6 : 0 :0;
		
		return t.intValue();
	}
	
	public boolean isGPSEnabled() {
		return m_gps != null ? m_gps.isGPSEnabled: false;
	}
	
	public double getLatitude() {
		return m_gps != null ? m_gps.getLatitude() : 0;
	}
	
	public double getLongitude() {
		return m_gps != null ? m_gps.getLongitude() :0;
	}
	
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
	public String getJSONHeader() {
		
		if (m_header != null)
			return m_header;
		
		String header = "";
		
    	JSONObject json = new JSONObject();

    	try {
      	 json.put("VERSION",RECORD_FORMAT_VERSION);
      	 json.put("DEVICE_NAME",this.getDeviceName());
      	 json.put("DEVICE_ID",  m_locActivity.getDeviceId());
      	 header = json.toString();
    	} catch(Exception ex) {
    	   // TODO Exception	
    	}
        return header;
	}

	@Override
	public void onSensorChanged(SensorEvent arg0) {
		
		// TODO Auto-generated method stub
		Sensor s=	arg0.sensor;
		
		Log.i("sensor type %d\n",""+s.getType());
  
		int ret = mSense.addSensorVal(s.getType(), arg0.values, getSpeed());

		if (ret == 0)
			return;
		
		if (ret == 1) {
			try{
			IOHttp.writeToFile(g_fileName, g_header, mSense.flushCurrent(m_gps, m_locActivity,TYPE_AUTO));
			}
		    catch(Exception e) {
		       //TODO Exception
		    }
		}
	}
	
	private String getDeviceName() {
		  String manufacturer = Build.MANUFACTURER;
		  String model = Build.MODEL;
		  if (model.startsWith(manufacturer)) {
		    return capitalize(model);
		  } else {
		    return capitalize(manufacturer) + " " + model;
		  }
		}


		private String capitalize(String s) {
		  if (s == null || s.length() == 0) {
		    return "";
		  }
		  char first = s.charAt(0);
		  if (Character.isUpperCase(first)) {
		    return s;
		  } else {
		    return Character.toUpperCase(first) + s.substring(1);
		  }
		}

	public String flushCurrent(String type) {
			return mSense.flushCurrent(m_gps,m_locActivity,type);
		}
}

class mySensor 
{

	private static float[][] m_acc;  // M accelerometer readings
	private static long[] m_acc_tim;  // time at which these readings were recorded
	private static float[][] m_mag;  // magenetic field readings
	private static long[] m_mag_tim; 
	private static int M_VAL = 3;
	private static int m_acc_head = 0;
	private static int m_acc_tail = 1;
	private static int m_mag_head = 0;
	private static int m_mag_tail = 1;

    private static CircularBuffer speedBuf;
    private static long speedRecordTime;
	private long lastAutoRecordTime;
	public mySensor(int m_val) {
		M_VAL = m_val;
		
		m_acc = new float[M_VAL][3];
		m_acc_tim = new long[M_VAL];
		
		m_mag = new float[M_VAL][3];
		m_mag_tim = new long[M_VAL];
	   
		speedBuf = new CircularBuffer(15);
		speedRecordTime = System.currentTimeMillis();
        lastAutoRecordTime = System.currentTimeMillis();
	}
	
	public int addSensorVal(int type, float[] val, int speed) {

		if (type == Sensor.TYPE_ACCELEROMETER) {
			//record speed every second
			if (System.currentTimeMillis() - speedRecordTime  >= 1000) {
				speedRecordTime = System.currentTimeMillis();
				speedBuf.add(speed+"");
			}
			
			// record the current value at the tail
			m_acc[m_acc_tail][0] = val[0]; m_acc[m_acc_tail][1] = val[1]; m_acc[m_acc_tail][2] = val[2];
			
			if (true) {
				for(int i = 0 ; i < 3; i++) {
				  	  if (Math.abs(m_acc[m_acc_tail][i] - m_acc[m_acc_head][i]) > 2.0 &&
				  			  m_acc[m_acc_head][i] != 0) {
				  		  
				  		  return 1;  
				  	  }
				}
			}
			
			m_acc[m_acc_head][0] = val[0]; m_acc[m_acc_head][1] = val[1]; m_acc[m_acc_head][2] = val[2];   
			
		} else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			m_mag[m_mag_head][0] = m_mag[m_mag_tail][0]; m_mag[m_mag_head][1] = m_mag[m_mag_tail][1]; 
			m_mag[m_mag_head][2] = m_mag[m_mag_tail][2];
			
			m_mag[m_mag_tail][0] = val[0]; m_mag[m_mag_tail][1] = val[1]; m_mag[m_mag_tail][2] = val[2];
			Log.i("mag :",val[0]+val[1]+val[2]+"");

			return 0;
		}
		
		return 0;
	}

    public String flushCurrent(GPSTracker gps, LocationActivity locActivity, String  type) {
		// compute the R I and Inclination using m_acc_tail-1 and m_mag_tail-1
    	float [] R = new float[9];
    	float [] I = new float[9];
    	int at = m_acc_tail;
    	int mt = m_mag_tail;
    	boolean ret = SensorManager.getRotationMatrix(R, I, m_acc[at], m_mag[mt]);
    	float [] O = new float[3];
    	String tmp = "";
		DecimalFormat df3=new DecimalFormat("0.000");
		DecimalFormat dfspeed = new DecimalFormat("0.0");
    	float inc = SensorManager.getInclination(I);
    	SensorManager.getOrientation(R, O);
    	
    	long currentTime = System.currentTimeMillis();
    	
    	//ma.setVal(m_acc[at], m_mag[mt], 0);
    	
    	JSONObject json = new JSONObject();
    	try{
    		
    	json.put("LATITUDE",gps.getLatitude());
    	json.put("LONGITUDE",gps.getLongitude());
    	int speed = (int)(gps.getLocation().getSpeed()*3.6);
    	json.put("SPEED", speed);
    	json.put("BEFORE_SPEED", speedBuf.toString());
    	json.put("LOCATION_METHOD", gps.getLocationMethod());
    	json.put("ACCURACY", gps.getLocation().getAccuracy());
    	tmp = "";
    	for (int i = 0 ; i < 3; i++)
    		tmp += ","+df3.format(m_acc[at][i]);
    	
     	json.put("ACCELEROMETER_NEW", tmp);
     	
     	tmp = "";
       	for (int i = 0 ; i < 3; i++)
    		tmp += ","+df3.format(m_acc[m_acc_head][i]);
       	json.put("ACCELROMETER_OLD", tmp);

       	tmp = "";
       	for (int i = 0 ; i < 3; i++)
    		tmp += ","+df3.format(m_mag[mt][i]);
       	json.put("MAGNETIC_NEW",  tmp );

       	tmp = "";
       	for (int i = 0 ; i < 9; i++)
    		tmp += ","+df3.format(R[i]);
       	json.put("ROTATION_MATRIX", tmp);

       	tmp = "";
       	for (int i = 0 ; i < 9; i++)
    		tmp += ","+df3.format(I[i]);
       	json.put("IDENT_MATRIX", tmp);
       	
       	tmp = "";
       	for (int i = 0 ; i < 3; i++)
    		tmp += ","+df3.format(O[i]);
       	json.put("ORIENTATION", tmp);

       	json.put("INCLINATION", df3.format(inc));
       	json.put("BEARING", gps.getLocation().getBearing());
       	json.put("DATE", new SimpleDateFormat("dd:MM:yyyy:HH:mm:ss.SSS").format(new Date()));
       	
       	json.put("TYPE", type);
       	
       	tmp = json.toString();
    
       	
       	if (type == SensorBg.TYPE_AUTO)
         	lastAutoRecordTime = currentTime;

    	} catch(Exception e) {
    		//TODO Exception
    		Log.i("flush",e.getMessage());
    	}
    	
    	locActivity.markJSONRecord(tmp);
    	
    	return tmp;
	}
}

class CircularBuffer {
	private int m_bufferSize;
	private String [] m_buffer;
	private int m_head;
	private int m_tail;
	
	public CircularBuffer(int size){
		m_bufferSize = size;
		if (size > 0) {
			m_buffer = new String[m_bufferSize];
		}
		m_head = m_tail = 0;
	}
	public void add(String val) {
        
		// initial case
		if (m_head == m_tail && m_head == 0) {
			m_buffer[m_tail] = val;
			m_tail = (m_tail + 1) % m_bufferSize;
		    return;
		}
		// wrap around case;
		if (((m_tail + 1)% m_bufferSize ) == m_head)
		{
			m_head = (m_head+1) % m_bufferSize;
			m_buffer[m_tail] = val;
			m_tail = (m_tail + 1) % m_bufferSize;
		    return;
		}
		
		// other cases
		m_buffer[m_tail] = val;
		m_tail = (m_tail +1 )% m_bufferSize;
		
	}
	
	public String getHead() {
       return m_buffer[m_head];		
	}
	
	public String getTail() {
		return m_buffer[m_tail];
	}
	
	public String toString() {
		String s = "";
		for (int i = 0 ; i < m_bufferSize; i++) {
			
			if ((m_head+i)%m_bufferSize == m_tail) {
				break;
			}
		    s += ","+ m_buffer[(m_head + i)%m_bufferSize];
			
		}
		return s;
	}
}



class myTimer extends TimerTask
{
	private String m_fileName;
	private SensorBg m_sensor;
    myTimer(String fileName, SensorBg sensor) {
    	
    	m_sensor = sensor;
    	m_fileName = fileName;
    }
    @Override

    public void run() {
       try {
    	   IOHttp.writeToFile(m_fileName, m_sensor.getJSONHeader(), m_sensor.flushCurrent(SensorBg.TYPE_HEARTBEAT));
    	   
       } catch(Exception ex) {
    	   // TODO Exception
       }
    }
}